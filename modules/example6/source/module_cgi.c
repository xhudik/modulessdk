// **************************************************************************
//
// CGI script of User Module
//
// Copyright (C) 2015 Conel s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include <stdio.h>

#include "um_html.h"

#include "module.h"

// **************************************************************************
// main function of CGI script "index.cgi"
int main(void)
{
  um_html_page_begin(MODULE_TITLE);

  um_html_form_begin(MODULE_TITLE, "Custom SNMP Module", NULL, 0, NULL, NULL);

  um_html_pre_head("Testing command");

  um_html_pre_text("snmpwalk -v 1 -c &lt;community&gt; &lt;IP address&gt; .1.3.6.1.3");

  um_html_form_end(NULL);

  um_html_page_end();

  return 0;
}

