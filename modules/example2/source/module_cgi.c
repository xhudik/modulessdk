// **************************************************************************
//
// CGI script of User Module
//
// Copyright (C) 2015 Conel s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include <stdio.h>

// **************************************************************************
// main function of CGI script "hello.cgi"
int main(void)
{
  // send HTTP header
  printf("Content-Type: text/html\n"
         "Cache-Control: no-cache\n"
         "Pragma: no-cache\n\n");

  // send HTML page
  printf("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n"
         "<html>\n"
         "<title>Example 2</title>\n"
         "<body>\n"
         "Hello World!\n"
         "<br><br>\n"
         "<a href=\"index.html\">Return</a>\n"
         "</body>\n"
         "</html>\n");

  return 0;
}

