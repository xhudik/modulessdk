// **************************************************************************
//
// CGI script of User Module
//
// **************************************************************************

#define _GNU_SOURCE

#include <stdio.h>

#include "um_html.h"

#include "module.h"

// **************************************************************************
// main function of CGI script "index.cgi"
int main(void)
{
  um_html_page_begin(MODULE_TITLE);

  um_html_form_begin(MODULE_TITLE, "Insert text here", NULL, 0, NULL, NULL);

  /* INSERT CODE HERE */

  um_html_form_end(NULL);

  um_html_page_end();

  return 0;
}

