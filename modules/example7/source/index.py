#!/usr/bin/python

# **************************************************************************
#
# CGI script of User Module
#
# Copyright (C) 2016 Conel s.r.o.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# **************************************************************************

import um

MODULE_TITLE = "Example 7"

um.html_page_begin(MODULE_TITLE)

um.html_form_begin(MODULE_TITLE, "System Status", None, 0, None, None)

um.html_pre_head("Binary Input")

msg  = "Binary input <b>BIN0</b> is <b>"
msg += "OFF" if um.gpio_get_bin0() else "ON"
msg += "</b>."

um.html_pre_text(msg)

um.html_pre_head("Binary Output")

msg  = "Binary output <b>OUT0</b> is <b>"
msg += "ON" if um.gpio_get_out0() else "OFF"
msg += "</b>."

um.html_pre_text(msg)

um.html_pre_head("Supply Voltage")

voltage = (um.gpio_get_voltage() + 50) / 100

msg  = "Supply voltage is <b>"
msg += str(voltage / 10.0) + " V" if voltage > 0 else "N/A"
msg += "</b>."

um.html_pre_text(msg)

um.html_pre_head("Temperature")

temperature = um.gpio_get_temperature()

msg  = "Internal temperature is <b>"
msg += str(temperature - 273) + " &degC" if temperature > 0 else "N/A"
msg += "</b>."

um.html_pre_text(msg)

um.html_form_end(None)

um.html_page_end()

