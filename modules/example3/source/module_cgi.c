// **************************************************************************
//
// CGI script of User Module
//
// Copyright (C) 2015 Conel s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include <stdio.h>
#include <string.h>

#include "um_gpio.h"
#include "um_html.h"

#include "module.h"

// **************************************************************************
// main function of CGI script "index.cgi"
int main(void)
{
  char                  msg[64];
  char                  value[32];
  int                   temperature;
  int                   voltage;

  um_html_page_begin(MODULE_TITLE);

  um_html_form_begin(MODULE_TITLE, "System Status", NULL, 0, NULL, NULL);

  um_html_pre_head("Binary Input");

  snprintf(msg, sizeof(msg), "Binary input <b>BIN0</b> is <b>%s</b>.",
          um_gpio_get_bin0() ? "OFF" : "ON");

  um_html_pre_text(msg);

  um_html_pre_head("Binary Output");

  snprintf(msg, sizeof(msg), "Binary output <b>OUT0</b> is <b>%s</b>.",
           um_gpio_get_out0() ? "ON" : "OFF");

  um_html_pre_text(msg);

  um_html_pre_head("Supply Voltage");

  voltage = (um_gpio_get_voltage() + 50) / 100;
  if (voltage > 0) {
    snprintf(value, sizeof(value), "%d.%d V", voltage / 10, voltage % 10);
  } else {
    strncpy(value, "N/A", sizeof(value));
  }

  snprintf(msg, sizeof(msg), "Supply voltage is <b>%s</b>.", value);

  um_html_pre_text(msg);

  um_html_pre_head("Temperature");

  temperature = um_gpio_get_temperature();
  if (temperature > 0) {
    snprintf(value, sizeof(value), "%d &deg;C", temperature - 273);
  } else {
    strncpy(value, "N/A", sizeof(value));
  }

  snprintf(msg, sizeof(msg), "Internal temperature is <b>%s</b>.", value);

  um_html_pre_text(msg);

  um_html_form_end(NULL);

  um_html_page_end();

  return 0;
}

