// **************************************************************************
//
// CGI scripts of User Module
//
// Copyright (C) 2015 Conel s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include <stdio.h>
#include <string.h>

#include "um_cgi.h"
#include "um_html.h"
#include "um_process.h"

#include "module.h"
#include "module_cfg.h"

// **************************************************************************

// structure of main menu
static const um_menu_item_t MENU[] = {
  { "Status"        , NULL               },
  { "System Log"    , "slog.cgi"         },
  { "Configuration" , NULL               },
  { "Expansion Port", "index.cgi"        },
  { "Customization" , NULL               },
  { "Return"        , "../../module.cgi" },
  { NULL            , NULL               }
};

// options for parameter "Expansion Port"
static const um_option_str_t DEVICE[] = {
  { "PORT1", "ttyS0" },
  { "PORT2", "ttyS1" },
  { NULL   , NULL    }
};

// options for parameter "Baudrate"
static const um_option_int_t BAUDRATE[] = {
  { "300"   , 300    },
  { "600"   , 600    },
  { "1200"  , 1200   },
  { "2400"  , 2400   },
  { "4800"  , 4800   },
  { "9600"  , 9600   },
  { "19200" , 19200  },
  { "38400" , 38400  },
  { "57600" , 57600  },
  { "115200", 115200 },
  { NULL    , 0      }
};

// options for parameter "Data Bits"
static const um_option_int_t DATABITS[] = {
  { "8" , 8 },
  { "7" , 7 },
  { NULL, 0 }
};

// options for parameter "Parity"
static const um_option_str_t PARITY[] = {
  { "none", "N"  },
  { "even", "E"  },
  { "odd" , "O"  },
  { NULL  , NULL }
};

// options for parameter "Stop Bits"
static const um_option_int_t STOPBITS[] = {
  { "1" , 1 },
  { "2" , 2 },
  { NULL, 0 }
};

// **************************************************************************

// the first part of JavaScript code
static const char *JAVASCRIPT_BEGIN =
  "function CheckForm() {\n"
  "  if (!IsValidPort(document.f.port, !document.f.enabled.checked)) {\n"
  "    return Error(\"Missing or invalid TCP port.\", document.f.port);\n"
  "  }\n"
  "  return true;\n"
  "}\n";

// the second part of JavaScript code
static const char *JAVASCRIPT_END =
  "document.f.onsubmit = CheckForm;\n"
  "document.f.enabled.focus();\n";

// **************************************************************************
// main function of CGI script "index.cgi"
static void main_index(void)
{
  module_cfg_t          cfg;

  module_cfg_load(&cfg);

  um_html_page_begin(MODULE_TITLE);

  um_html_form_begin(MODULE_TITLE, "Expansion Port Configuration", "set.cgi", 0, JAVASCRIPT_BEGIN, MENU);

  um_html_table(2, 0);

  um_html_check_box("enabled", cfg.enabled);
  um_html_text("Enable expansion port access over TCP");

  um_html_table(2, 100);

  um_html_text("Expansion Port");
  um_html_select_str("device", cfg.device, DEVICE);

  um_html_text("Baudrate");
  um_html_select_int("baudrate", cfg.baudrate, BAUDRATE);

  um_html_text("Data Bits");
  um_html_select_int("databits", cfg.databits, DATABITS);

  um_html_text("Parity");
  um_html_select_str("parity", cfg.parity, PARITY);

  um_html_text("Stop Bits");
  um_html_select_int("stopbits", cfg.stopbits, STOPBITS);

  um_html_text("TCP Port");
  um_html_input_int("port", cfg.port, 0, NULL);

  um_html_form_break();

  um_html_table(1, 0);

  um_html_submit("button", "Apply");

  um_html_form_end(JAVASCRIPT_END);

  um_html_page_end();
}

// **************************************************************************
// main function of CGI script "set.cgi"
static void main_set(void)
{
  module_cfg_t          cfg;
  int                   ok, input_ok;

  um_cgi_begin();

  um_html_page_begin(MODULE_TITLE);

  ok       = 0;
  input_ok = um_cgi_query_ok()                             &&
             um_cgi_get_bool("enabled" , &cfg.enabled    ) &&
             um_cgi_get_str ("device"  , &cfg.device  , 1) &&
             um_cgi_get_int ("baudrate", &cfg.baudrate   ) &&
             um_cgi_get_int ("databits", &cfg.databits   ) &&
             um_cgi_get_str ("parity"  , &cfg.parity  , 1) &&
             um_cgi_get_int ("stopbits", &cfg.stopbits   ) &&
             um_cgi_get_int ("port"    , &cfg.port       );

  if (input_ok) {
    if (module_cfg_save(&cfg)) {
      ok = !um_process_exec(MODULE_INIT, "restart");
    }
  }

  um_html_config_info_box(ok, input_ok, 0, "index.cgi");

  um_html_page_end();

  um_cgi_end();
}

// **************************************************************************
// main function of CGI script "slog.cgi"
static void main_slog(void)
{
  um_html_system_log(MODULE_TITLE, MENU);
}

// **************************************************************************
// main function
int main(int argc, char *argv[])
{
  const char            *name;

  if (argc > 0) {
    name = basename(argv[0]);
    if (!strcmp(name, "index.cgi")) {
      main_index();
    } else if (!strcmp(name, "set.cgi")) {
      main_set();
    } else if (!strcmp(name, "slog.cgi")) {
      main_slog();
    }
  }

  return 0;
}

