// **************************************************************************
//
// Definitions for User Module
//
// Copyright (C) 2015 Conel s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#ifndef _MODULE_H_
#define _MODULE_H_

// module title
#define MODULE_TITLE        "Example 5"

// module name
#define MODULE_NAME         "example5"

// prefix for all parameters
#define MODULE_PREFIX       "MOD_EXAMPLE5_"

// name of configuration file
#define MODULE_SETTINGS     "/opt/" MODULE_NAME "/etc/settings"

// name of init script
#define MODULE_INIT         "/opt/" MODULE_NAME "/etc/init"

#endif

