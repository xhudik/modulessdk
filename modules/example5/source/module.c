// **************************************************************************
//
// Daemon of User Module
//
// Copyright (C) 2015 Conel s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <syslog.h>
#include <signal.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <sys/socket.h>

#include "um_com.h"
#include "um_lock.h"
#include "um_macros.h"
#include "um_socket.h"

#include "module.h"
#include "module_cfg.h"

// **************************************************************************

// received signal number
volatile int got_signal = 0;

// **************************************************************************
// callback function for handling signals
static void sig_handler(int signum)
{
  // store signal number
  got_signal = signum;
}

// **************************************************************************
// main function
int main(int argc, char *argv[])
{
  struct sockaddr_in    addr;
  fd_set                read_fd_set;
  socklen_t             size;
  module_cfg_t          cfg;
  unsigned char         buffer[256];
  char                  device[16];
  int                   sk_listen;
  int                   sk_data;
  int                   sk_temp;
  int                   fd_com;
  ssize_t               count;

  UNUSED(argc);

  // create new session
  setsid();

  // install signal handler
  signal(SIGINT, sig_handler);
  signal(SIGTERM, sig_handler);
  signal(SIGQUIT, sig_handler);

  // open system log
  openlog(basename(argv[0]), LOG_PID | LOG_NDELAY, LOG_DAEMON);

  // write message to system log
  syslog(LOG_NOTICE, "started");

  // load configuration
  module_cfg_load(&cfg);

  // open serial port
  snprintf(device, sizeof(device), "/dev/%s", cfg.device);
  if (!um_lock_acquire(device, 1)) {
    syslog(LOG_ERR, "device %s is locked", device);
    exit(1);
  }
  fd_com = um_com_open(device, cfg.baudrate, cfg.databits, cfg.parity, cfg.stopbits);
  if (fd_com < 0) {
    syslog(LOG_ERR, "unable to open %s", device);
    exit(1);
  }

  // initialize sockets
  sk_data   = -1;
  sk_listen = um_socket_open_tcp_server(cfg.port);
  if (sk_listen < 0) {
    syslog(LOG_ERR, "unable to create TCP server on port %u", cfg.port);
    exit(1);
  }

  // main loop
  while (!got_signal) {

    // wait for event
    FD_ZERO(&read_fd_set);
    FD_SET(fd_com, &read_fd_set);
    FD_SET(sk_listen, &read_fd_set);
    if (sk_data >= 0) {
      FD_SET(sk_data, &read_fd_set);
    }
    if (select(FD_SETSIZE, &read_fd_set, NULL, NULL, NULL) < 0) {
      continue;
    }

    // accept new TCP connection
    if (FD_ISSET(sk_listen, &read_fd_set)) {
      size = sizeof(addr);
      sk_temp = accept(sk_listen, (struct sockaddr *)&addr, &size);
      if (sk_temp >= 0) {
        if (sk_data >= 0) {
          um_socket_close(sk_data);
          syslog(LOG_NOTICE, "TCP connection closed");
        }
        sk_data = sk_temp;
        syslog(LOG_NOTICE, "TCP connection from %s established", inet_ntoa(addr.sin_addr));
      } else {
        syslog(LOG_ERR, "accept error: %s", strerror(errno));
      }
    }

    // receive data from TCP connection
    if (sk_data >= 0 && FD_ISSET(sk_data, &read_fd_set)) {
      count = recv(sk_data, buffer, sizeof(buffer), 0);
      if (count > 0) {
        if (write(fd_com, buffer, (size_t)count) < 0) {
          syslog(LOG_ERR, "write error: %s", strerror(errno));
        }
      } else {
        if (count < 0) {
          syslog(LOG_ERR, "recv error: %s", strerror(errno));
        }
        um_socket_close(sk_data);
        sk_data = -1;
        syslog(LOG_NOTICE, "TCP connection closed");
      }
    }

    // receive data from serial port
    if (FD_ISSET(fd_com, &read_fd_set)) {
      count = read(fd_com, buffer, sizeof(buffer));
      if (count > 0 && sk_data >= 0) {
        send(sk_data, buffer, (size_t)count, MSG_NOSIGNAL);
      }
    }

  }

  // write message to system log
  syslog(LOG_NOTICE, "stopped");

  // exit
  return 0;
}

