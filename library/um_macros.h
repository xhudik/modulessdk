// **************************************************************************
//
// Macros
//
// Copyright (C) 2015 Conel s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#ifndef _UM_MACROS_H_
#define _UM_MACROS_H_

// macro for suppressing "unused variable" warning
#define UNUSED(x) ((void)(x))

// macro for suppressing "ignoring return value" warning
#define UNCHECKED(x) if (x) {}

// macro for getting number of array elements
#define SIZEOF(x) (sizeof(x) / sizeof(x[0]))

// macro for converting a code fragment into a string constant
#define STRINGIFY(x...) STRINGIFY_INDIRECT(x)
#define STRINGIFY_INDIRECT(x...) #x

#endif

