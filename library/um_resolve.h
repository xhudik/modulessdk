// **************************************************************************
//
// Function for resolving host IP address
//
// Copyright (C) 2015 Conel s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#ifndef _UM_RESOLVE_H_
#define _UM_RESOLVE_H_

#ifdef __cplusplus
extern "C" {
#endif

// resolve host IP address
extern unsigned int um_resolve(const char *hostname);

#ifdef __cplusplus
}
#endif

#endif

