// **************************************************************************
//
// Functions for sending file via HTTP protocol
//
// Copyright (C) 2015 Conel s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include <stdio.h>

#include "um_http.h"

// **************************************************************************
// start file transfer
void um_http_file_begin(const char *filename)
{
  printf("Content-Type: application/force-download\n"
         "Content-Encoding: AnyTrash\n"
         "Content-Description: File Transfer\n"
         "Content-Disposition: attachment; filename=\"%s\"\n\n", filename);
}

// **************************************************************************
// send content of file
void um_http_file_content(const char *filename, const char *error)
{
  FILE                  *file_ptr;
  char                  line[128];
  int                   empty;

  empty = 1;
  if ((file_ptr = fopen(filename, "r"))) {
    while (fgets(line, sizeof(line), file_ptr)) {
      fputs(line, stdout);
      empty = 0;
    }
    fclose(file_ptr);
  }

  if (empty && error) {
    printf("%s\n", error);
  }
}

// **************************************************************************
// finish file transfer
void um_http_file_end(void)
{
  fflush(stdout);
}

