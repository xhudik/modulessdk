// **************************************************************************
//
// Functions for reading out status
//
// Copyright (C) 2017 Conel s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "um_status.h"
#include "um_process.h"
#include "um_systime.h"

// **************************************************************************

// auxiliary macros
#define MOBILE(module) ((module) ? "mobile 2" : "mobile")
#define MODULE(module) ((module) ? "module 2" : "module")
#define MWAN(module)   ((module) ? "ppp 2"    : "ppp"   )

// **************************************************************************
// execute the program "status" and return its output
static const char *um_status_exec(const char *category)
{
  static __thread char   cached_output[4096];
  static __thread char   cached_category[32];
  static __thread time_t cached_validity;
  ssize_t                count;
  int                    fd[2];

  // return the cached output if it is valid
  if (!strcmp(cached_category, category) && cached_validity >= um_systime()) {
    return cached_output;
  }

  // create a pipe
  if (pipe(fd) != 0) {
    return "";
  }

  // execute the program "status" and redirect its output to the pipe
  um_process_redirect_stdout(fd[1]);
  if (um_process_exec("/usr/bin/status", "-v", category) != 0) {
    close(fd[0]);
    return "";
  }

  // read what the program sent to the output
  count = read(fd[0], cached_output, sizeof(cached_output) - 1);

  // close the pipe
  close(fd[0]);

  // check count of read bytes
  if (count < 0) {
    return "";
  }

  // terminate the string in the buffer
  cached_output[count] = '\0';

  // set category of cached output
  snprintf(cached_category, sizeof(cached_category), "%s", category);

  // set validity of cached output
  cached_validity = um_systime() + 1;

  // return the read output
  return cached_output;
}

// **************************************************************************
// get the value of the given status parameter
char *um_status_get(const char *category, const char *param)
{
  static __thread char  value[128];
  const char            *act_ptr;
  const char            *end_ptr;

  // get output of the program "status" for the given category
  act_ptr = um_status_exec(category);

  // find the parameter value in the output
  while (*act_ptr) {
    if (*act_ptr == '\n') {
      act_ptr++;
    }
    end_ptr = strchr(act_ptr, '\n') ? : (act_ptr + strlen(act_ptr));
    if (strncmp(act_ptr, param, strlen(param))) {
      act_ptr = end_ptr;
      continue;
    }
    act_ptr = strstr(act_ptr, ": ");
    if (!act_ptr) {
      break;
    }
    act_ptr += 2;
    if (act_ptr >= end_ptr || act_ptr + sizeof(value) <= end_ptr) {
      break;
    }
    snprintf(value, (size_t)(end_ptr - act_ptr + 1), "%s", act_ptr);
    return value;
  }

  // return "N/A" in case that the parameter does not exists
  snprintf(value, sizeof(value), "N/A");
  return value;
}

// **************************************************************************
// get the product name
const char *um_status_get_product_name(void)
{
  static __thread char  value[32];

  snprintf(value, sizeof(value), "%s", um_status_get("sys", "Product Name"));

  return value;
}

// **************************************************************************
// get the firmware version
const char *um_status_get_firmware_version(void)
{
  static __thread char  value[32];

  snprintf(value, sizeof(value), "%s", um_status_get("sys", "Firmware Version"));

  return value;
}

// **************************************************************************
// get the serial number
const char *um_status_get_serial_number(void)
{
  static __thread char  value[32];

  snprintf(value, sizeof(value), "%s", um_status_get("sys", "Serial Number"));

  return value;
}

// **************************************************************************
// get the model of the given module
const char *um_status_get_module_model(int module)
{
  static __thread char  value[32];

  snprintf(value, sizeof(value), "%s", um_status_get(MODULE(module), "Model"));

  return value;
}

// **************************************************************************
// get the revision of the given module
const char *um_status_get_module_revision(int module)
{
  static __thread char  value[32];

  snprintf(value, sizeof(value), "%s", um_status_get(MODULE(module), "Revision"));

  return value;
}

// **************************************************************************
// get the IMEI of the given module
const char *um_status_get_module_imei(int module)
{
  static __thread char  value[32];

  snprintf(value, sizeof(value), "%s", um_status_get(MODULE(module), "IMEI"));

  return value;
}

// **************************************************************************
// get the ICCID of the given module
const char *um_status_get_module_iccid(int module)
{
  static __thread char  value[32];

  snprintf(value, sizeof(value), "%s", um_status_get(MODULE(module), "ICCID"));

  return value;
}

// **************************************************************************
//  get the ESN of the given module
const char *um_status_get_module_esn(int module)
{
  static __thread char  value[32];

  snprintf(value, sizeof(value), "%s", um_status_get(MODULE(module), "ESN"));

  return value;
}

// **************************************************************************
// get the MEID of the given module
const char *um_status_get_module_meid(int module)
{
  static __thread char  value[32];

  snprintf(value, sizeof(value), "%s", um_status_get(MODULE(module), "MEID"));

  return value;
}

// **************************************************************************
// get the interface of the given module
const char *um_status_get_module_iface(int module)
{
  static __thread char  value[32];

  snprintf(value, sizeof(value), "%s", um_status_get(MWAN(module), "Interface"));

  return value;
}

// **************************************************************************
// get the registration state of the given module
const char *um_status_get_mobile_registration(int module)
{
  static __thread char  value[32];

  snprintf(value, sizeof(value), "%s", um_status_get(MOBILE(module), "Registration"));

  return value;
}

// **************************************************************************
// get the operator of the given module
const char *um_status_get_mobile_operator(int module)
{
  static __thread char  value[32];

  snprintf(value, sizeof(value), "%s", um_status_get(MOBILE(module), "Operator"));

  return value;
}

// **************************************************************************
// get the technology of the given module
const char *um_status_get_mobile_technology(int module)
{
  static __thread char  value[32];

  snprintf(value, sizeof(value), "%s", um_status_get(MOBILE(module), "Technology"));

  return value;
}

// **************************************************************************
// get the PLMN of the given module
unsigned int um_status_get_mobile_plmn(int module)
{
  return (unsigned int)strtoul(um_status_get(MOBILE(module), "PLMN"), NULL, 10);
}

// **************************************************************************
// get the cell of the given module
unsigned int um_status_get_mobile_cell(int module)
{
  return (unsigned int)strtoul(um_status_get(MOBILE(module), "Cell"), NULL, 16);
}

// **************************************************************************
// get the LAC of the given module
unsigned int um_status_get_mobile_lac(int module)
{
  return (unsigned int)strtoul(um_status_get(MOBILE(module), "LAC"), NULL, 16);
}

// **************************************************************************
// get the channel of the given module
unsigned int um_status_get_mobile_channel(int module)
{
  return (unsigned int)strtoul(um_status_get(MOBILE(module), "Channel"), NULL, 10);
}

// **************************************************************************
// get the signal strength of the given module
int um_status_get_mobile_strength(int module)
{
  return atoi(um_status_get(MOBILE(module), "Signal Strength"));
}

// **************************************************************************
// get the signal quality of the given module
int um_status_get_mobile_quality(int module)
{
  return atoi(um_status_get(MOBILE(module), "Signal Quality"));
}

