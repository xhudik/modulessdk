// **************************************************************************
//
// Functions for work with sockets
//
// Copyright (C) 2015 Conel s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include <errno.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <sys/socket.h>
#include <sys/un.h>

#include "um_socket.h"

// **************************************************************************
// open TCP socket (server mode)
int um_socket_open_tcp_server(unsigned int port)
{
  struct sockaddr_in    addr;
  int                   sock;
  int                   opt;

  // create new socket
  if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
    syslog(LOG_ERR, "create socket error: %s", strerror(errno));
    return -1;
  }

  // enable reusing addresses
  opt = 1;
  if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) != 0) {
    syslog(LOG_ERR, "setsockopt(SO_REUSEADDR) error: %s", strerror(errno));
  }

  // assign local IP address and TCP port
  memset(&addr, 0, sizeof(addr));
  addr.sin_family      = AF_INET;
  addr.sin_port        = htons((uint16_t)port);
  addr.sin_addr.s_addr = INADDR_ANY;
  if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)) != 0) {
    syslog(LOG_ERR, "bind socket error: %s", strerror(errno));
    close(sock);
    return -1;
  }

  // listen for incoming TCP connection requests
  if (listen(sock, 8) != 0) {
    syslog(LOG_ERR, "listen on socket error: %s", strerror(errno));
    close(sock);
    return -1;
  }

  // return socket descriptor
  return sock;
}

// **************************************************************************
// open TCP socket (client mode)
int um_socket_open_tcp_client(unsigned int ip, unsigned int port)
{
  struct sockaddr_in    addr;
  int                   sock;

  // create new socket
  if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
    syslog(LOG_ERR, "create socket error: %s", strerror(errno));
    return -1;
  }

  // connect to remote IP address and TCP port
  memset(&addr, 0, sizeof(addr));
  addr.sin_family      = AF_INET;
  addr.sin_port        = htons((uint16_t)port);
  addr.sin_addr.s_addr = htonl((uint32_t)ip);
  if (connect(sock, (struct sockaddr *)&addr, sizeof(addr)) != 0) {
    syslog(LOG_ERR, "connect socket error: %s", strerror(errno));
    close(sock);
    return -1;
  }

  // return socket descriptor
  return sock;
}

// **************************************************************************
// open UDP socket (server mode)
int um_socket_open_udp_server(unsigned int port)
{
  struct sockaddr_in    addr;
  int                   sock;

  // create new socket
  if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
    syslog(LOG_ERR, "create socket error: %s", strerror(errno));
    return -1;
  }

  // assign local IP address and UDP port
  memset(&addr, 0, sizeof(addr));
  addr.sin_family      = AF_INET;
  addr.sin_port        = htons((uint16_t)port);
  addr.sin_addr.s_addr = INADDR_ANY;
  if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)) != 0) {
    syslog(LOG_ERR, "bind socket error: %s", strerror(errno));
    close(sock);
    return -1;
  }

  // return socket descriptor
  return sock;
}

// **************************************************************************
// open UDP socket (client mode)
int um_socket_open_udp_client(unsigned int ip, unsigned int port)
{
  struct sockaddr_in    addr;
  int                   sock;

  // create new socket
  if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
    syslog(LOG_ERR, "create socket error: %s", strerror(errno));
    return -1;
  }

  // assign local IP address and UDP port
  memset(&addr, 0, sizeof(addr));
  addr.sin_family      = AF_INET;
  addr.sin_port        = 0;
  addr.sin_addr.s_addr = INADDR_ANY;
  if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)) != 0) {
    syslog(LOG_ERR, "bind socket error: %s", strerror(errno));
    close(sock);
    return -1;
  }

  // assign remote IP address and UDP port
  memset(&addr, 0, sizeof(addr));
  addr.sin_family      = AF_INET;
  addr.sin_port        = htons((uint16_t)port);
  addr.sin_addr.s_addr = htonl((uint32_t)ip);
  if (connect(sock, (struct sockaddr *)&addr, sizeof(addr)) != 0) {
    syslog(LOG_ERR, "connect socket error: %s", strerror(errno));
    close(sock);
    return -1;
  }

  // return socket descriptor
  return sock;
}

// **************************************************************************
// open UNIX socket (server mode)
int um_socket_open_unix_server(const char *path)
{
  struct sockaddr_un    addr;
  int                   sock;

  // create new socket
  if ((sock = socket(AF_UNIX, SOCK_STREAM, 0)) < 0) {
    syslog(LOG_ERR, "create socket error: %s", strerror(errno));
    return -1;
  }

  // bind socket to path
  memset(&addr, 0, sizeof(addr));
  addr.sun_family = AF_UNIX;
  strncpy(addr.sun_path, path, sizeof(addr.sun_path) - 1);
  unlink(addr.sun_path);
  if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)) != 0) {
    syslog(LOG_ERR, "bind socket error: %s", strerror(errno));
    close(sock);
    return -1;
  }

  // listen for incoming connection requests
  if (listen(sock, 8) != 0) {
    syslog(LOG_ERR, "listen on socket error: %s", strerror(errno));
    close(sock);
    return -1;
  }

  // return socket descriptor
  return sock;
}

// **************************************************************************
// open UNIX socket (client mode)
int um_socket_open_unix_client(const char *path)
{
  struct sockaddr_un    addr;
  int                   sock;

  // create new socket
  if ((sock = socket(AF_UNIX, SOCK_STREAM, 0)) < 0) {
    syslog(LOG_ERR, "create socket error: %s", strerror(errno));
    return -1;
  }

  // connect to server
  memset(&addr, 0, sizeof(addr));
  addr.sun_family = AF_UNIX;
  strncpy(addr.sun_path, path, sizeof(addr.sun_path) - 1);
  if (connect(sock, (struct sockaddr *)&addr, sizeof(addr)) != 0) {
    syslog(LOG_ERR, "connect socket error: %s", strerror(errno));
    close(sock);
    return -1;
  }

  // return socket descriptor
  return sock;
}

// **************************************************************************
// close socket
void um_socket_close(int sock)
{
  if (close(sock) != 0) {
    syslog(LOG_ERR, "close socket error: %s", strerror(errno));
  }
}

// **************************************************************************
// enable keepalive on TCP socket
void um_socket_keepalive(int sock, int time, int intvl, int probes)
{
  int                   opt;

  // enable keepalive
  opt = 1;
  if (setsockopt(sock, SOL_SOCKET, SO_KEEPALIVE, &opt, sizeof(opt)) != 0) {
    syslog(LOG_ERR, "setsockopt(SO_KEEPALIVE) error: %s", strerror(errno));
  }

  // set keepalive parameters
  if (setsockopt(sock, SOL_TCP, TCP_KEEPIDLE, &time, sizeof(time)) != 0) {
    syslog(LOG_ERR, "setsockopt(TCP_KEEPIDLE) error: %s", strerror(errno));
  }
  if (setsockopt(sock, SOL_TCP, TCP_KEEPINTVL, &intvl, sizeof(intvl)) != 0) {
    syslog(LOG_ERR, "setsockopt(TCP_KEEPINVTL) error: %s", strerror(errno));
  }
  if (setsockopt(sock, SOL_TCP, TCP_KEEPCNT, &probes, sizeof(probes)) != 0) {
    syslog(LOG_ERR, "setsockopt(TCP_KEEPCNT) error: %s", strerror(errno));
  }
}

