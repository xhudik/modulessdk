// **************************************************************************
//
// Functions for work with lock files
//
// Copyright (C) 2015 Conel s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/stat.h>

#include "um_lock.h"
#include "um_process.h"
#include "um_systime.h"

// **************************************************************************
// check existence of stale lock file
static void um_lock_check(const char *filename)
{
  struct stat           stat_buf;
  FILE                  *file_ptr;
  pid_t                 pid;

  // check existence of lock file
  if (stat(filename, &stat_buf) != 0) {
    return;
  }

  // open lock file
  if (!(file_ptr = fopen(filename, "r"))) {
    return;
  }

  // read process ID from lock file
  if (fscanf(file_ptr, "%d", &pid) != 1) {
    pid = 0;
  }

  // close lock file
  fclose(file_ptr);

  // check existence of process
  if (um_process_exists(pid)) {
    return;
  }

  // delete lock file
  unlink(filename);
}

// **************************************************************************
// create new lock file
static int um_lock_create(const char *filename)
{
  char                  buffer[16];
  int                   result;
  int                   fd;

  // open lock file
  if ((fd = open(filename, O_EXCL | O_CREAT | O_WRONLY, 0644)) < 0) {
    return 0;
  }

  // write process ID to lock file
  snprintf(buffer, sizeof(buffer), "%10d\n", getpid());
  result = write(fd, buffer, strlen(buffer)) >= 0;

  // close file
  close(fd);

  // return result of operation
  return result;
}

// **************************************************************************
// acquire lock
int um_lock_acquire(const char *device, int tout)
{
  struct timespec       requested, remaining;
  time_t                timeout;
  char                  filename[64];

  // prepare filename
  snprintf(filename, sizeof(filename), "/var/lock/LCK..%s", basename(device));

  // try to create lock file
  timeout = um_systime() + tout;
  do {
    // check existence of stale lock file
    um_lock_check(filename);
    // create new lock file
    if (um_lock_create(filename)) {
      // operation succeeded
      return 1;
    }
    // sleep 100 ms
    requested.tv_sec  = 0;
    requested.tv_nsec = 100000000UL;
    nanosleep(&requested, &remaining);
  } while (um_systime() <= timeout);

  // operation failed
  return 0;
}

// **************************************************************************
// release lock
void um_lock_release(const char *device)
{
  char                  filename[64];

  // prepare filename
  snprintf(filename, sizeof(filename), "/var/lock/LCK..%s", basename(device));

  // delete lock file
  unlink(filename);
}

