// **************************************************************************
//
// Functions for getting parameters of CGI script
//
// Copyright (C) 2015 Conel s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#ifndef _UM_CGI_H_
#define _UM_CGI_H_

#ifdef __cplusplus
extern "C" {
#endif

// start CGI script parameters processing
extern void um_cgi_begin(void);

// finish CGI script parameters processing
extern void um_cgi_end(void);

// check if query is ok
extern int um_cgi_query_ok(void);

// get CGI script parameter (raw string)
extern int um_cgi_get_raw(const char *name, char **value_ptr);

// get CGI script parameter (string)
extern int um_cgi_get_str(const char *name, char **value_ptr, int no_spaces);

// get CGI script parameter (integer)
extern int um_cgi_get_int(const char *name, unsigned int *value_ptr);

// get CGI script parameter (boolean)
extern int um_cgi_get_bool(const char *name, unsigned int *value_ptr);

// get CGI script parameter (IP address)
extern int um_cgi_get_ip(const char *name, unsigned int *value_ptr, int zero);

// get CGI script parameter (MAC address)
extern int um_cgi_get_mac(const char *name, char **value_ptr, int zero);

#ifdef __cplusplus
}
#endif

#endif

