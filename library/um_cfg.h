// **************************************************************************
//
// Functions for work with configuration
//
// Copyright (C) 2015 Conel s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#ifndef _UM_CFG_H_
#define _UM_CFG_H_

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

// open configuration file
extern FILE *um_cfg_open(const char *config_name, const char *open_mode);

// close configuration file
extern void um_cfg_close(FILE *file_ptr);

// put string into configuration file
extern void um_cfg_put_str(FILE *file_ptr, const char *name, const char *value);

// put integer into configuration file
extern void um_cfg_put_int(FILE *file_ptr, const char *name, unsigned int value, int store_zero);

// put boolean into configuration file
extern void um_cfg_put_bool(FILE *file_ptr, const char *name, unsigned int value);

// put IP address into configuration file
extern void um_cfg_put_ip(FILE *file_ptr, const char *name, unsigned int value);

// get string from configuration file (allocate memory for result)
extern char *um_cfg_get_str(FILE *file_ptr, const char *name);

// get integer from configuration file
extern unsigned int um_cfg_get_int(FILE *file_ptr, const char *name);

// get IP address from configuration file
extern unsigned int um_cfg_get_ip(FILE *file_ptr, const char *name);

#ifdef __cplusplus
}
#endif

#endif

