// **************************************************************************
//
// Function for getting monotonic system time
//
// Copyright (C) 2015 Conel s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#ifndef _UM_SYSTIME_H_
#define _UM_SYSTIME_H_

#include <time.h>

#ifdef __cplusplus
extern "C" {
#endif

// get monotonic system time
extern time_t um_systime(void);

#ifdef __cplusplus
}
#endif

#endif

