# **************************************************************************
#
# Python ctypes-based wrapper
#
# Copyright (C) 2016 Conel s.r.o.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# **************************************************************************

from ctypes import *
from os.path import dirname, abspath, sep

lib = cdll.LoadLibrary(dirname(abspath(__file__)) + sep + "libum.so")

class File(Structure):
    pass

class MenuItem(Structure):
    _fields_ = ("label", c_char_p), ("url", c_char_p)

class OptionStr(Structure):
    _fields_ = ("name", c_char_p), ("value", c_char_p)

class OptionInt(Structure):
    _fields_ = ("name", c_char_p), ("value", c_uint)

class MenuItemArray:
    def from_param(self, param):
        if not param:
            return None
        param.append(MenuItem(None, None))
        return ((MenuItem) * len(param))(*param)

class OptionStrArray:
    def from_param(self, param):
        param.append(OptionStr(None, None))
        return ((OptionStr) * len(param))(*param)

class OptionIntArray:
    def from_param(self, param):
        param.append(OptionInt(None, 0))
        return ((OptionInt) * len(param))(*param)

GPIO_PORT_TYPE_EMPTY  = 0x00
GPIO_PORT_TYPE_RS232  = 0x02
GPIO_PORT_TYPE_RS485  = 0x03
GPIO_PORT_TYPE_MBUS   = 0x04
GPIO_PORT_TYPE_CNT    = 0x05
GPIO_PORT_TYPE_ETH    = 0x08
GPIO_PORT_TYPE_WMBUS  = 0x0A
GPIO_PORT_TYPE_RS422  = 0x0B
GPIO_PORT_TYPE_NONE   = 0x0F
GPIO_PORT_TYPE_WIFI   = 0x11
GPIO_PORT_TYPE_SDCARD = 0x12
GPIO_PORT_TYPE_DUST   = 0x13
GPIO_PORT_TYPE_SWITCH = 0x20
GPIO_PORT_TYPE_DUST2  = 0x21

XCCNT_FEATURE_NONE    = 0x0000
XCCNT_FEATURE_AN1     = 0x0001
XCCNT_FEATURE_AN2     = 0x0002
XCCNT_FEATURE_CNT1    = 0x0004
XCCNT_FEATURE_CNT2    = 0x0008
XCCNT_FEATURE_BIN1    = 0x0010
XCCNT_FEATURE_BIN2    = 0x0020
XCCNT_FEATURE_BIN3    = 0x0040
XCCNT_FEATURE_BIN4    = 0x0080
XCCNT_FEATURE_OUT1    = 0x0100
XCCNT_FEATURE_FEEDER  = 0x0200
XCCNT_FEATURE_BICNT   = 0x0400

base64_encode_str = lib.um_base64_encode_str
base64_encode_str.argtypes = [c_char_p]
base64_encode_str.restype = c_char_p

base64_decode_str = lib.um_base64_decode_str
base64_decode_str.argtypes = [c_char_p]
base64_decode_str.restype = c_char_p

cfg_open = lib.um_cfg_open
cfg_open.argtypes = [c_char_p, c_char_p]
cfg_open.restype = POINTER(File)

cfg_close = lib.um_cfg_close
cfg_close.argtypes = [POINTER(File)]
cfg_close.restype = None

cfg_put_str = lib.um_cfg_put_str
cfg_put_str.argtypes = [POINTER(File), c_char_p, c_char_p]
cfg_put_str.restype = None

cfg_put_int = lib.um_cfg_put_int
cfg_put_int.argtypes = [POINTER(File), c_char_p, c_uint, c_int]
cfg_put_int.restype = None

cfg_put_bool = lib.um_cfg_put_bool
cfg_put_bool.argtypes = [POINTER(File), c_char_p, c_uint]
cfg_put_bool.restype = None

cfg_put_ip = lib.um_cfg_put_ip
cfg_put_ip.argtypes = [POINTER(File), c_char_p, c_uint]
cfg_put_ip.restype = None

cfg_get_str = lib.um_cfg_get_str
cfg_get_str.argtypes = [POINTER(File), c_char_p]
cfg_get_str.restype = c_char_p

cfg_get_int = lib.um_cfg_get_int
cfg_get_int.argtypes = [POINTER(File), c_char_p]
cfg_get_int.restype = c_uint

cfg_get_ip = lib.um_cfg_get_ip
cfg_get_ip.argtypes = [POINTER(File), c_char_p]
cfg_get_ip.restype = c_uint

cgi_begin = lib.um_cgi_begin
cgi_begin.argtypes = []
cgi_begin.restype = None

cgi_end = lib.um_cgi_end
cgi_end.argtypes = []
cgi_end.restype = None

cgi_query_ok = lib.um_cgi_query_ok
cgi_query_ok.argtypes = []
cgi_query_ok.restype = c_int

_cgi_get_raw = lib.um_cgi_get_raw
_cgi_get_raw.argtypes = [c_char_p, POINTER(c_char_p)]
_cgi_get_raw.restype = c_int

def cgi_get_raw(name):
    result = c_char_p()
    if not _cgi_get_raw(name, byref(result)):
        raise ValueError()
    return result.value

_cgi_get_str = lib.um_cgi_get_str
_cgi_get_str.argtypes = [c_char_p, POINTER(c_char_p), c_int]
_cgi_get_str.restype = c_int

def cgi_get_str(name, no_spaces):
    result = c_char_p()
    if not _cgi_get_str(name, byref(result), no_spaces):
        raise ValueError()
    return result.value

_cgi_get_int = lib.um_cgi_get_int
_cgi_get_int.argtypes = [c_char_p, POINTER(c_uint)]
_cgi_get_int.restype = c_int

def cgi_get_int(name):
    result = c_uint()
    if not _cgi_get_int(name, byref(result)):
        raise ValueError()
    return result.value

_cgi_get_bool = lib.um_cgi_get_bool
_cgi_get_bool.argtypes = [c_char_p, POINTER(c_uint)]
_cgi_get_bool.restype = c_int

def cgi_get_bool(name):
    result = c_uint()
    if not _cgi_get_bool(name, byref(result)):
        raise ValueError()
    return result.value

_cgi_get_ip = lib.um_cgi_get_ip
_cgi_get_ip.argtypes = [c_char_p, POINTER(c_uint), c_int]
_cgi_get_ip.restype = c_int

def cgi_get_ip(name, zero):
    result = c_uint()
    if not _cgi_get_ip(name, byref(result), zero):
        raise ValueError()
    return result.value

_cgi_get_mac = lib.um_cgi_get_mac
_cgi_get_mac.argtypes = [c_char_p, POINTER(c_char_p), c_int]
_cgi_get_mac.restype = c_int

def cgi_get_mac(name, zero):
    result = c_char_p()
    if not _cgi_get_mac(name, byref(result), zero):
        raise ValueError()
    return result.value

com_open = lib.um_com_open
com_open.argtypes = [c_char_p, c_uint, c_uint, c_char_p, c_uint]
com_open.restype = c_int

com_close = lib.um_com_close
com_close.argtypes = [c_int]
com_close.restype = None

com_xmit = lib.um_com_xmit
com_xmit.argtypes = [c_int, c_char_p, c_int]
com_xmit.restype = c_char_p

com_drain = lib.um_com_drain
com_drain.argtypes = [c_int]
com_drain.restype = None

com_set_rts = lib.um_com_set_rts
com_set_rts.argtypes = [c_int, c_int]
com_set_rts.restype = None

com_set_dtr = lib.um_com_set_dtr
com_set_dtr.argtypes = [c_int, c_int]
com_set_dtr.restype = None

com_get_cts = lib.um_com_get_cts
com_get_cts.argtypes = [c_int]
com_get_cts.restype = c_int

com_get_dsr = lib.um_com_get_dsr
com_get_dsr.argtypes = [c_int]
com_get_dsr.restype = c_int

com_get_cd = lib.um_com_get_cd
com_get_cd.argtypes = [c_int]
com_get_cd.restype = c_int

file_exists = lib.um_file_exists
file_exists.argtypes = [c_char_p]
file_exists.restype = c_int

file_time = lib.um_file_time
file_time.argtypes = [c_char_p]
file_time.restype = c_long

file_size = lib.um_file_size
file_size.argtypes = [c_char_p]
file_size.restype = c_long

file_gets = lib.um_file_gets
file_gets.argtypes = [c_char_p]
file_gets.restype = c_char_p

file_puts = lib.um_file_puts
file_puts.argtypes = [c_char_p, c_char_p]
file_puts.restype = c_int

gpio_get_module_idx = lib.um_gpio_get_module_idx
gpio_get_module_idx.argtypes = []
gpio_get_module_idx.restype = c_int

gpio_get_module_type = lib.um_gpio_get_module_type
gpio_get_module_type.argtypes = [c_int]
gpio_get_module_type.restype = c_int

gpio_get_module_sim = lib.um_gpio_get_module_sim
gpio_get_module_sim.argtypes = [c_int]
gpio_get_module_sim.restype = c_int

gpio_set_led_usr = lib.um_gpio_set_led_usr
gpio_set_led_usr.argtypes = [c_uint]
gpio_set_led_usr.restype = None

gpio_get_port_type = lib.um_gpio_get_port_type
gpio_get_port_type.argtypes = [c_int]
gpio_get_port_type.restype = c_int

gpio_set_port_sd = lib.um_gpio_set_port_sd
gpio_set_port_sd.argtypes = [c_int, c_uint]
gpio_set_port_sd.restype = None

gpio_get_port_sd = lib.um_gpio_get_port_sd
gpio_get_port_sd.argtypes = [c_int]
gpio_get_port_sd.restype = c_int

gpio_set_port_output = lib.um_gpio_set_port_output
gpio_set_port_output.argtypes = [c_int, c_uint, c_uint]
gpio_set_port_output.restype = None

gpio_get_port_input = lib.um_gpio_get_port_input
gpio_get_port_input.argtypes = [c_int, c_uint]
gpio_get_port_input.restype = c_int

gpio_get_mbus_overload = lib.um_gpio_get_mbus_overload
gpio_get_mbus_overload.argtypes = [c_int]
gpio_get_mbus_overload.restype = c_int

gpio_set_out0 = lib.um_gpio_set_out0
gpio_set_out0.argtypes = [c_uint]
gpio_set_out0.restype = None

gpio_set_out1 = lib.um_gpio_set_out1
gpio_set_out1.argtypes = [c_uint]
gpio_set_out1.restype = None

gpio_get_out0 = lib.um_gpio_get_out0
gpio_get_out0.argtypes = []
gpio_get_out0.restype = c_int

gpio_get_out1 = lib.um_gpio_get_out1
gpio_get_out1.argtypes = []
gpio_get_out1.restype = c_int

gpio_get_bin0 = lib.um_gpio_get_bin0
gpio_get_bin0.argtypes = []
gpio_get_bin0.restype = c_int

gpio_get_bin1 = lib.um_gpio_get_bin1
gpio_get_bin1.argtypes = []
gpio_get_bin1.restype = c_int

gpio_has_out0 = lib.um_gpio_has_out0
gpio_has_out0.argtypes = []
gpio_has_out0.restype = c_int

gpio_has_out1 = lib.um_gpio_has_out1
gpio_has_out1.argtypes = []
gpio_has_out1.restype = c_int

gpio_has_bin0 = lib.um_gpio_has_bin0
gpio_has_bin0.argtypes = []
gpio_has_bin0.restype = c_int

gpio_has_bin1 = lib.um_gpio_has_bin1
gpio_has_bin1.argtypes = []
gpio_has_bin1.restype = c_int

gpio_get_temperature = lib.um_gpio_get_temperature
gpio_get_temperature.argtypes = []
gpio_get_temperature.restype = c_int

gpio_get_voltage = lib.um_gpio_get_voltage
gpio_get_voltage.argtypes = []
gpio_get_voltage.restype = c_int

html_page_begin = lib.um_html_page_begin
html_page_begin.argtypes = [c_char_p]
html_page_begin.restype = None

html_page_end = lib.um_html_page_end
html_page_end.argtypes = []
html_page_end.restype = None

html_info_box = lib.um_html_info_box
html_info_box.argtypes = [c_int, c_char_p, c_char_p, c_char_p, c_char_p]
html_info_box.restype = None

html_config_info_box = lib.um_html_config_info_box
html_config_info_box.argtypes = [c_int, c_int, c_int, c_char_p]
html_config_info_box.restype = None

html_form_begin = lib.um_html_form_begin
html_form_begin.argtypes = [c_char_p, c_char_p, c_char_p, c_int, c_char_p, MenuItemArray()]
html_form_begin.restype = None

html_form_end = lib.um_html_form_end
html_form_end.argtypes = [c_char_p]
html_form_end.restype = None

html_form_break = lib.um_html_form_break
html_form_break.argtypes = []
html_form_break.restype = None

html_table = lib.um_html_table
html_table.argtypes = [c_uint, c_uint]
html_table.restype = None

html_id = lib.um_html_id
html_id.argtypes = [c_char_p]
html_id.restype = None

html_style = lib.um_html_style
html_style.argtypes = [c_char_p]
html_style.restype = None

html_text = lib.um_html_text
html_text.argtypes = [c_char_p]
html_text.restype = None

html_link = lib.um_html_link
html_link.argtypes = [c_char_p, c_char_p]
html_link.restype = None

html_input_str = lib.um_html_input_str
html_input_str.argtypes = [c_char_p, c_char_p]
html_input_str.restype = None

html_input_str_ro = lib.um_html_input_str_ro
html_input_str_ro.argtypes = [c_char_p, c_char_p]
html_input_str_ro.restype = None

html_input_str_hd = lib.um_html_input_str_hd
html_input_str_hd.argtypes = [c_char_p, c_char_p]
html_input_str_hd.restype = None

html_input_int = lib.um_html_input_int
html_input_int.argtypes = [c_char_p, c_uint, c_int, c_char_p]
html_input_int.restype = None

html_input_ip = lib.um_html_input_ip
html_input_ip.argtypes = [c_char_p, c_uint]
html_input_ip.restype = None

html_input_pwd = lib.um_html_input_pwd
html_input_pwd.argtypes = [c_char_p]
html_input_pwd.restype = None

html_input_file = lib.um_html_input_file
html_input_file.argtypes = [c_char_p]
html_input_file.restype = None

html_select_str = lib.um_html_select_str
html_select_str.argtypes = [c_char_p, c_char_p, OptionStrArray()]
html_select_str.restype = None

html_select_int = lib.um_html_select_int
html_select_int.argtypes = [c_char_p, c_uint, OptionIntArray()]
html_select_int.restype = None

html_radio = lib.um_html_radio
html_radio.argtypes = [c_char_p, c_uint, c_uint]
html_radio.restype = None

html_check_box = lib.um_html_check_box
html_check_box.argtypes = [c_char_p, c_uint]
html_check_box.restype = None

html_area_text = lib.um_html_area_text
html_area_text.argtypes = [c_char_p, c_char_p, c_uint, c_uint]
html_area_text.restype = None

html_area_file = lib.um_html_area_file
html_area_file.argtypes = [c_char_p, c_char_p, c_uint, c_uint]
html_area_file.restype = None

html_submit = lib.um_html_submit
html_submit.argtypes = [c_char_p, c_char_p]
html_submit.restype = None

html_pre_head = lib.um_html_pre_head
html_pre_head.argtypes = [c_char_p]
html_pre_head.restype = None

html_pre_text = lib.um_html_pre_text
html_pre_text.argtypes = [c_char_p]
html_pre_text.restype = None

html_pre_file = lib.um_html_pre_file
html_pre_file.argtypes = [c_char_p, c_char_p]
html_pre_file.restype = None

html_pre_proc = lib.um_html_pre_proc
html_pre_proc.argtypes = [c_char_p]
html_pre_proc.restype = None

html_system_log = lib.um_html_system_log
html_system_log.argtypes = [c_char_p, MenuItemArray()]
html_system_log.restype = None

http_file_begin = lib.um_http_file_begin
http_file_begin.argtypes = [c_char_p]
http_file_begin.restype = None

http_file_content = lib.um_http_file_content
http_file_content.argtypes = [c_char_p, c_char_p]
http_file_content.restype = None

http_file_end = lib.um_http_file_end
http_file_end.argtypes = []
http_file_end.restype = None

lock_acquire = lib.um_lock_acquire
lock_acquire.argtypes = [c_char_p, c_int]
lock_acquire.restype = c_int

lock_release = lib.um_lock_release
lock_release.argtypes = [c_char_p]
lock_release.restype = None

process_redirect_stdin = lib.um_process_redirect_stdin
process_redirect_stdin.argtypes = [c_int]
process_redirect_stdin.restype = None

process_redirect_stdout = lib.um_process_redirect_stdout
process_redirect_stdout.argtypes = [c_int]
process_redirect_stdout.restype = None

process_redirect_stderr = lib.um_process_redirect_stderr
process_redirect_stderr.argtypes = [c_int]
process_redirect_stderr.restype = None

process_exec = lib.um_process_exec
process_exec.argtypes = [c_char_p]
process_exec.restype = c_int

process_start = lib.um_process_start
process_start.argtypes = [c_char_p]
process_start.restype = c_int

process_kill = lib.um_process_kill
process_kill.argtypes = [c_int, c_int]
process_kill.restype = c_int

process_exists = lib.um_process_exists
process_exists.argtypes = [c_int]
process_exists.restype = c_int

resolve = lib.um_resolve
resolve.argtypes = [c_char_p]
resolve.restype = c_uint

socket_open_tcp_server = lib.um_socket_open_tcp_server
socket_open_tcp_server.argtypes = [c_uint]
socket_open_tcp_server.restype = c_int

socket_open_tcp_client = lib.um_socket_open_tcp_client
socket_open_tcp_client.argtypes = [c_uint, c_uint]
socket_open_tcp_client.restype = c_int

socket_open_udp_server = lib.um_socket_open_udp_server
socket_open_udp_server.argtypes = [c_uint]
socket_open_udp_server.restype = c_int

socket_open_udp_client = lib.um_socket_open_udp_client
socket_open_udp_client.argtypes = [c_uint, c_uint]
socket_open_udp_client.restype = c_int

socket_open_unix_server = lib.um_socket_open_unix_server
socket_open_unix_server.argtypes = [c_char_p]
socket_open_unix_server.restype = c_int

socket_open_unix_client = lib.um_socket_open_unix_client
socket_open_unix_client.argtypes = [c_char_p]
socket_open_unix_client.restype = c_int

socket_close = lib.um_socket_close
socket_close.argtypes = [c_int]
socket_close.restype = None

socket_keepalive = lib.um_socket_keepalive
socket_keepalive.argtypes = [c_int, c_int, c_int, c_int]
socket_keepalive.restype = None

status_get = lib.um_status_get
status_get.argtypes = [c_char_p, c_char_p]
status_get.restype = c_char_p

status_get_product_name = lib.um_status_get_product_name
status_get_product_name.argtypes = []
status_get_product_name.restype = c_char_p

status_get_firmware_version = lib.um_status_get_firmware_version
status_get_firmware_version.argtypes = []
status_get_firmware_version.restype = c_char_p

status_get_serial_number = lib.um_status_get_serial_number
status_get_serial_number.argtypes = []
status_get_serial_number.restype = c_char_p

status_get_module_model = lib.um_status_get_module_model
status_get_module_model.argtypes = [c_int]
status_get_module_model.restype = c_char_p

status_get_module_revision = lib.um_status_get_module_revision
status_get_module_revision.argtypes = [c_int]
status_get_module_revision.restype = c_char_p

status_get_module_imei = lib.um_status_get_module_imei
status_get_module_imei.argtypes = [c_int]
status_get_module_imei.restype = c_char_p

status_get_module_iccid = lib.um_status_get_module_iccid
status_get_module_iccid.argtypes = [c_int]
status_get_module_iccid.restype = c_char_p

status_get_module_esn = lib.um_status_get_module_esn
status_get_module_esn.argtypes = [c_int]
status_get_module_esn.restype = c_char_p

status_get_module_meid = lib.um_status_get_module_meid
status_get_module_meid.argtypes = [c_int]
status_get_module_meid.restype = c_char_p

status_get_module_iface = lib.um_status_get_module_iface
status_get_module_iface.argtypes = [c_int]
status_get_module_iface.restype = c_char_p

status_get_mobile_registration = lib.um_status_get_mobile_registration
status_get_mobile_registration.argtypes = [c_int]
status_get_mobile_registration.restype = c_char_p

status_get_mobile_operator = lib.um_status_get_mobile_operator
status_get_mobile_operator.argtypes = [c_int]
status_get_mobile_operator.restype = c_char_p

status_get_mobile_technology = lib.um_status_get_mobile_technology
status_get_mobile_technology.argtypes = [c_int]
status_get_mobile_technology.restype = c_char_p

status_get_mobile_plmn = lib.um_status_get_mobile_plmn
status_get_mobile_plmn.argtypes = [c_int]
status_get_mobile_plmn.restype = c_uint

status_get_mobile_cell = lib.um_status_get_mobile_cell
status_get_mobile_cell.argtypes = [c_int]
status_get_mobile_cell.restype = c_uint

status_get_mobile_lac = lib.um_status_get_mobile_lac
status_get_mobile_lac.argtypes = [c_int]
status_get_mobile_lac.restype = c_uint

status_get_mobile_channel = lib.um_status_get_mobile_channel
status_get_mobile_channel.argtypes = [c_int]
status_get_mobile_channel.restype = c_uint

status_get_mobile_strength = lib.um_status_get_mobile_strength
status_get_mobile_strength.argtypes = [c_int]
status_get_mobile_strength.restype = c_int

status_get_mobile_quality = lib.um_status_get_mobile_quality
status_get_mobile_quality.argtypes = [c_int]
status_get_mobile_quality.restype = c_int

systime = lib.um_systime
systime.argtypes = []
systime.restype = c_long

_xccnt_get_features = lib.um_xccnt_get_features
_xccnt_get_features.argtypes = [c_int, POINTER(c_ushort)]
_xccnt_get_features.restype = c_int

def xccnt_get_features(port):
    result = c_ushort()
    if not _xccnt_get_features(port, byref(result)):
        raise IOError()
    return result.value

_xccnt_get_inputs = lib.um_xccnt_get_inputs
_xccnt_get_inputs.argtypes = [c_int, POINTER(c_ushort)]
_xccnt_get_inputs.restype = c_int

def xccnt_get_inputs(port):
    result = c_ushort()
    if not _xccnt_get_inputs(port, byref(result)):
        raise IOError()
    return result.value

_xccnt_get_input = lib.um_xccnt_get_input
_xccnt_get_input.argtypes = [c_int, c_int, POINTER(c_ushort)]
_xccnt_get_input.restype = c_int

def xccnt_get_input(port, idx):
    result = c_ushort()
    if not _xccnt_get_input(port, idx, byref(result)):
        raise IOError()
    return result.value

_xccnt_get_analog = lib.um_xccnt_get_analog
_xccnt_get_analog.argtypes = [c_int, c_int, POINTER(c_ushort)]
_xccnt_get_analog.restype = c_int

def xccnt_get_analog(port, idx):
    result = c_ushort()
    if not _xccnt_get_analog(port, idx, byref(result)):
        raise IOError()
    return result.value

_xccnt_get_counter = lib.um_xccnt_get_counter
_xccnt_get_counter.argtypes = [c_int, c_int, POINTER(c_uint)]
_xccnt_get_counter.restype = c_int

def xccnt_get_counter(port, idx):
    result = c_uint()
    if not _xccnt_get_counter(port, idx, byref(result)):
        raise IOError()
    return result.value

_xccnt_set_counter = lib.um_xccnt_set_counter
_xccnt_set_counter.argtypes = [c_int, c_int, c_uint]
_xccnt_set_counter.restype = c_int

def xccnt_set_counter(port, idx, value):
    if not _xccnt_set_counter(port, idx, value):
        raise IOError()
    return value

_xccnt_get_output = lib.um_xccnt_get_output
_xccnt_get_output.argtypes = [c_int, POINTER(c_ushort)]
_xccnt_get_output.restype = c_int

def xccnt_get_output(port):
    result = c_ushort()
    if not _xccnt_get_output(port, byref(result)):
        raise IOError()
    return result.value

_xccnt_set_output = lib.um_xccnt_set_output
_xccnt_set_output.argtypes = [c_int, c_ushort]
_xccnt_set_output.restype = c_int

def xccnt_set_output(port, value):
    if not _xccnt_set_output(port, value):
        raise IOError()
    return value

_xccnt_impulse = lib.um_xccnt_impulse
_xccnt_impulse.argtypes = [c_int, c_ushort]
_xccnt_impulse.restype = c_int

def xccnt_impulse(port, cycles):
    if not _xccnt_impulse(port, cycles):
        raise IOError()
    return cycles

