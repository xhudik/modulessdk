// **************************************************************************
//
// Functions for work with XC-CNT expansion board
//
// Copyright (C) 2015 Conel s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#ifndef _UM_XCCNT_H_
#define _UM_XCCNT_H_

#ifdef __cplusplus
extern "C" {
#endif

// supported features
#define UM_XCCNT_FEATURE_NONE       0x0000
#define UM_XCCNT_FEATURE_AN1        0x0001
#define UM_XCCNT_FEATURE_AN2        0x0002
#define UM_XCCNT_FEATURE_CNT1       0x0004
#define UM_XCCNT_FEATURE_CNT2       0x0008
#define UM_XCCNT_FEATURE_BIN1       0x0010
#define UM_XCCNT_FEATURE_BIN2       0x0020
#define UM_XCCNT_FEATURE_BIN3       0x0040
#define UM_XCCNT_FEATURE_BIN4       0x0080
#define UM_XCCNT_FEATURE_OUT1       0x0100
#define UM_XCCNT_FEATURE_FEEDER     0x0200
#define UM_XCCNT_FEATURE_BICNT      0x0400

// get bitmask of supported features
extern int um_xccnt_get_features(int port, unsigned short *value_ptr);

// get state of binary inputs
extern int um_xccnt_get_inputs(int port, unsigned short *value_ptr);

// get state of binary input
extern int um_xccnt_get_input(int port, int idx, unsigned short *value_ptr);

// get state of analog input
extern int um_xccnt_get_analog(int port, int idx, unsigned short *value_ptr);

// get state of counter input
extern int um_xccnt_get_counter(int port, int idx, unsigned int *value_ptr);

// set state of counter input
extern int um_xccnt_set_counter(int port, int idx, unsigned int value);

// get state of binary output
extern int um_xccnt_get_output(int port, unsigned short *value_ptr);

// set state of binary output
extern int um_xccnt_set_output(int port, unsigned short value);

// create impulse on binary output
extern int um_xccnt_impulse(int port, unsigned short cycles);

#ifdef __cplusplus
}
#endif

#endif

