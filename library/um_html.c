// **************************************************************************
//
// Functions for generating HTML code
//
// Copyright (C) 2015 Conel s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "um_html.h"
#include "um_http.h"
#include "um_process.h"

// **************************************************************************

// flag indicating insertion of tag <form>
static unsigned int     um_table_form   = 0;

// current row in table
static unsigned int     um_table_row    = 0;

// current column in table
static unsigned int     um_table_col    = 0;

// number of columns in table
static unsigned int     um_table_cols   = 2;

// width of the first column in table
static unsigned int     um_table_align  = 0;

// ID for next cell in table
static const char       *um_table_id    = NULL;

// style for next cell in table
static const char       *um_table_style = NULL;

// **************************************************************************
// send HTML code of beginning of page
void um_html_page_begin(const char *title)
{
  printf("Content-Type: text/html\n"
         "Cache-Control: no-cache\n"
         "Pragma: no-cache\n"
         "\n"
         "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n"
         "<html>\n"
         "\n"
         "<head>\n"
         "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\n"
         "<meta http-equiv=\"Content-Style-Type\" content=\"text/css\">\n"
         "<meta http-equiv=\"Content-Script-Type\" content=\"text/javascript\">\n"
         "<title>%s</title>\n"
         "<style type=\"text/css\">\n"
         "<!--\n"
         "body, table, tr, td, a {\n"
         "  font-size: 12px; font-family: Verdana, Arial, Helvetica, sans-serif;\n"
         "}\n"
         "input, select, option, textarea, button {\n"
         "  font-size: 13.3333px;\n"
         "}\n"
         "form, input {\n"
         "  margin: 0px;\n"
         "}\n"
         "pre {\n"
         "  margin: 10px 0px 10px 0px;\n"
         "}\n"
         "h2 {\n"
         "  font-size: 24px; margin-bottom: 3px;\n"
         "}\n"
         "a {\n"
         "  color: #0033CC; text-decoration: none;\n"
         "}\n"
         "a:hover {\n"
         "  text-decoration: underline;\n"
         "}\n"
         ".logo {\n"
         "  color: #000080; text-shadow: #B0C0E0 2px 2px 2px; filter: shadow(color=#B0C0E0, direction=145, strength=4);\n"
         "}\n"
         ".window {\n"
         "  background-color: #F4F4F4;\n"
         "}\n"
         ".title {\n"
         "  color: #FFFFFF; background-color: #000080; border-color: #000080; font-weight: bold;\n"
         "}\n"
         ".header {\n"
         "  color: #000000; background-color: #C8D8FF;\n"
         "}\n"
         ".input {\n"
         "  width: 165px;\n"
         "}\n"
         "-->\n"
         "</style>\n"
         "</head>\n"
         "\n"
         "<body bgcolor=\"#FFFFFF\">\n", title);
}

// **************************************************************************
// send HTML code of end of page
void um_html_page_end(void)
{
  printf("</body>\n"
         "\n"
         "</html>\n");

  fflush(stdout);
}

// **************************************************************************
// send HTML code of info box (generic)
void um_html_info_box(int ok, const char *title, const char *text, const char *url, const char *button)
{
  printf("<center>\n"
         "<form name=\"f\" action=\"\">\n"
         "<table cellspacing=\"0\" cellpadding=\"5\" border=\"3\" class=\"window\">\n"
         "  <tr>\n"
         "    <td bgcolor=\"#%06X\" align=\"center\"><font color=\"#FFFFFF\"><b>%s</b></font></td>\n"
         "  </tr>\n"
         "  <tr>\n"
         "    <td>\n"
         "      <table>\n"
         "        <tr align=\"center\">\n"
         "          <td>%s</td>\n"
         "        </tr>\n"
         "        <tr align=\"center\">\n"
         "          <td id=\"button\"><a href=\"%s\">&raquo; %s &laquo;</a></td>\n"
         "        </tr>\n"
         "      </table>\n"
         "    </td>\n"
         "  </tr>\n"
         "</table>\n"
         "</form>\n"
         "</center>\n"
         "<script type=\"text/javascript\">\n"
         "<!--\n"
         "document.getElementById('button').innerHTML = '<input type=\"button\" name=\"button\" value=\"%s\" onclick=\"location.href=\\'%s\\'\">';\n"
         "document.f.button.focus();\n"
         "//-->\n"
         "</script>\n", ok ? 0x008000 : 0x800000, title, text, url, button, button, url);
}

// **************************************************************************
// send HTML code of info box (configuration)
void um_html_config_info_box(int ok, int input_ok, int reboot, const char *url)
{
  const char            *msg;

  if (ok) {
    if (reboot) {
      msg = "Configuration successfully updated. You must <a href=\"reboot_exec.cgi\">reboot</a> to apply changes.";
    } else {
      msg = "Configuration successfully updated.";
    }
  } else {
    if (input_ok) {
      msg = "Error during configuration update.";
    } else {
      msg = "Invalid input.";
    }
  }

  um_html_info_box(ok, "Configuration", msg, url, "Back");
}

// **************************************************************************
// send HTML code of beginning of form
void um_html_form_begin(const char *heading, const char *title, const char *action, int multipart, const char *javascript, const um_menu_item_t *menu)
{
  static const um_menu_item_t DEFAULT_MENU[] = {
    { "Customization", NULL               },
    { "Return"       , "../../module.cgi" },
    { NULL           , NULL               }
  };

  if (!menu) {
    menu = DEFAULT_MENU;
  }

  if (javascript) {
    printf("<script type=\"text/javascript\">\n"
           "<!--\n");
    if (strstr(javascript, "Error")) {
      printf("function Error(msg, obj) {\n"
             "  alert(msg); obj.focus(); obj.select(); return false;\n"
             "}\n"
             "function GetValue(obj) {\n"
             "  return obj.value.replace(/\\s/g, '');\n"
             "}\n");
    }
    if (strstr(javascript, "IsEmpty")) {
      printf("function IsEmpty(obj) {\n"
             "  return GetValue(obj).length == 0;\n"
             "}\n");
    }
    if (strstr(javascript, "IsInRange")) {
      printf("function IsInRange(obj, low, high, empty) {\n"
             "  var str = GetValue(obj);\n"
             "  if (empty && str.length == 0) return true;\n"
             "  return (parseInt(str) >= low) && (parseInt(str) <= high);\n"
             "}\n");
    }
    if (strstr(javascript, "IsValidMAC")) {
      printf("function IsValidMAC(obj, empty) {\n"
             "  var str = GetValue(obj);\n"
             "  if (empty && str.length == 0) return true;\n"
             "  return str.search(/^[0-9a-fA-F]{1,2}(:[0-9a-fA-F]{1,2}){5}$/) >= 0;\n"
             "}\n");
    }
    if (strstr(javascript, "IsValidIP")) {
      printf("function IsValidIP(obj, empty) {\n"
             "  var str = GetValue(obj);\n"
             "  if (empty && str.length == 0) return true;\n"
             "  var fields = str.match(/^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})$/);\n"
             "  if (fields != null) {\n"
             "    var tmp = fields[1] | fields[2] | fields[3] | fields[4];\n"
             "    return (tmp < 256) && (empty || tmp > 0);\n"
             "  } else {\n"
             "    return false;\n"
             "  }\n"
             "}\n");
    }
    if (strstr(javascript, "IsValidPort")) {
      printf("function IsValidPort(obj, empty) {\n"
             "  var str = GetValue(obj);\n"
             "  if (empty && str.length == 0) return true;\n"
             "  var value = parseInt(str);\n"
             "  return (value > 0) && (value < 65536);\n"
             "}\n");
    }
    printf("%s"
           "-->\n"
           "</script>\n", javascript);
  }

  printf("<table width=\"100%%\" cellspacing=\"2\" cellpadding=\"2\" border=\"0\">\n"
         "  <tr>\n"
         "    <td nowrap class=\"logo\"><h2>%s</h2></td>\n"
         "  </tr>\n"
         "</table>\n"
         "<table width=\"100%%\" cellspacing=\"2\" cellpadding=\"2\" border=\"0\">\n"
         "  <tr>\n"
         "    <td valign=\"top\" width=\"1%%\">\n"
         "      <table width=\"200\" cellspacing=\"0\" cellpadding=\"5\" border=\"3\" class=\"window\">\n", heading);

  while (menu->label) {
    printf("        <tr>\n"
           "          <td nowrap class=\"title\">%s</td>\n"
           "        </tr>\n"
           "        <tr>\n"
           "          <td>\n"
           "            <table cellspacing=\"2\" cellpadding=\"1\">\n", menu->label);
    while ((++menu)->url) {
      printf("              <tr><td nowrap><a href=\"%s\">%s</a></td></tr>\n", menu->url, menu->label);
    }
    printf("            </table>\n"
           "          </td>\n"
           "        </tr>\n");
  }

  printf("      </table>\n"
         "    </td>\n"
         "    <td valign=\"top\" width=\"99%%\">\n");

  if (action) {
    printf("      <form name=\"f\" action=\"%s\" method=\"post\"", action);
    if (multipart) {
      printf(" enctype=\"multipart/form-data\">\n");
    } else {
      printf(">\n");
    }
    um_table_form = 1;
  }

  printf("      <table width=\"100%%\" cellspacing=\"0\" cellpadding=\"5\" border=\"3\" class=\"window\">\n"
         "        <tr>\n"
         "          <td align=\"center\" class=\"title\">%s</td>\n"
         "        </tr>\n"
         "        <tr>\n"
         "          <td>", title);
}

// **************************************************************************
// send HTML code of end of form
void um_html_form_end(const char *javascript)
{
  if (um_table_col) {
    printf("</tr>");
  }
  if (um_table_row) {
    printf("</table>");
  }

  printf("</td>\n"
         "        </tr>\n"
         "      </table>\n");
  if (um_table_form) {
    printf("      </form>\n");
  }
  printf("    </td>\n"
         "  </tr>\n"
         "</table>\n");

  if (javascript) {
    printf("<script type=\"text/javascript\">\n"
           "<!--\n"
           "%s"
           "-->\n"
           "</script>\n", javascript);
  }
}

// **************************************************************************
// send HTML code of horizontal line in form
void um_html_form_break(void)
{
  if (um_table_col) {
    printf("</tr>");
    um_table_col = 0;
  }
  if (um_table_row) {
    printf("</table>");
    um_table_row = 0;
  }

  printf("</td></tr><tr><td>");
}

// **************************************************************************
// define structure of table
void um_html_table(unsigned int cols, unsigned int align)
{
  if (um_table_col) {
    printf("</tr>");
    um_table_col = 0;
  }
  if (um_table_row) {
    printf("</table>");
    um_table_row = 0;
  }

  um_table_cols  = cols;
  um_table_align = align;
}

// **************************************************************************
// define ID for next cell in table
void um_html_id(const char *name)
{
  um_table_id = name;
}

// **************************************************************************
// define style for next cell in table
void um_html_style(const char *style)
{
  um_table_style = style;
}

// **************************************************************************
// send HTML code of beginning of cell
static void um_html_cell_begin(void)
{
  if (!um_table_row) {
    printf("<table>");
  }
  if (!um_table_col) {
    printf("<tr>");
    um_table_row++;
  }

  if (um_table_align && !um_table_col && um_table_row == 1) {
    if (um_table_id) {
      printf("<td nowrap width=\"%u\" id=\"%s\">", um_table_align, um_table_id);
    } else {
      printf("<td nowrap width=\"%u\">", um_table_align);
    }
  } else {
    if (um_table_id) {
      printf("<td nowrap id=\"%s\">", um_table_id);
    } else {
      printf("<td nowrap>");
    }
  }
  um_table_col++;

  um_table_id = NULL;

  if (!um_table_style) {
    um_table_style = "class=\"input\"";
  }
}

// **************************************************************************
// send HTML code of end of cell
static void um_html_cell_end(void)
{
  if (um_table_col < um_table_cols) {
    printf("</td>");
  } else {
    printf("</td></tr>");
    um_table_col = 0;
  }

  um_table_style = NULL;
}

// **************************************************************************
// send HTML code of simple text
void um_html_text(const char *text)
{
  um_html_cell_begin();

  printf("%s", text);

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of link
void um_html_link(const char *url, const char *text)
{
  um_html_cell_begin();

  printf("<a href=\"%s\">%s</a>", url, text);

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of input box (string)
void um_html_input_str(const char *name, const char *value)
{
  um_html_cell_begin();

  printf("<input %s type=\"text\" name=\"%s\" value=\"%s\">", um_table_style, name, value);

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of input box (read only string)
void um_html_input_str_ro(const char *name, const char *value)
{
  um_html_cell_begin();

  printf("<input %s type=\"text\" name=\"%s\" value=\"%s\" disabled>", um_table_style, name, value);

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of input box (hidden string)
void um_html_input_str_hd(const char *name, const char *value)
{
  um_html_cell_begin();

  printf("<input type=\"hidden\" name=\"%s\" value=\"%s\">", name, value);

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of input box (integer)
void um_html_input_int(const char *name, unsigned int value, int show_zero, const char *unit)
{
  um_html_cell_begin();

  printf("<input %s type=\"text\" name=\"%s\" value=\"", um_table_style, name);
  if (value || show_zero) {
    printf("%u", value);
  }
  if (unit) {
    printf("\"> %s", unit);
  } else {
    printf("\">");
  }

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of input box (IP address)
void um_html_input_ip(const char *name, unsigned int value)
{
  um_html_cell_begin();

  printf("<input %s type=\"text\" name=\"%s\" value=\"", um_table_style, name);
  if (value) {
    printf("%u.%u.%u.%u\">",
           (value >> 24) & 0xFF,
           (value >> 16) & 0xFF,
           (value >> 8) & 0xFF,
           (value) & 0xFF);
  } else {
    printf("\">");
  }

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of input box (password)
void um_html_input_pwd(const char *name)
{
  um_html_cell_begin();

  printf("<input type=\"password\" name=\"%s\">", name);

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of input box (file)
void um_html_input_file(const char *name)
{
  um_html_cell_begin();

  printf("<input type=\"file\" name=\"%s\" size=\"50\">", name);

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of select box (string)
void um_html_select_str(const char *name, const char *value, const um_option_str_t *option_ptr)
{
  um_html_cell_begin();

  printf("<select %s name=\"%s\">", um_table_style, name);
  while (option_ptr->text) {
    printf("<option value=\"%s\"%s>%s", option_ptr->value, !strcmp(option_ptr->value, value) ? " selected" : "", option_ptr->text);
    option_ptr++;
  }
  printf("</select>");

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of select box (integer)
void um_html_select_int(const char *name, unsigned int value, const um_option_int_t *option_ptr)
{
  um_html_cell_begin();

  printf("<select %s name=\"%s\">", um_table_style, name);
  while (option_ptr->text) {
    printf("<option value=\"%u\"%s>%s", option_ptr->value, option_ptr->value == value ? " selected" : "", option_ptr->text);
    option_ptr++;
  }
  printf("</select>");

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of radio button
void um_html_radio(const char *name, unsigned int id, unsigned int value)
{
  um_html_cell_begin();

  printf("<input type=\"radio\" name=\"%s\" value=\"%u\"%s>", name, id, id == value ? " checked" : "");

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of check box
void um_html_check_box(const char *name, unsigned int checked)
{
  um_html_cell_begin();

  printf("<input type=\"checkbox\" name=\"%s\"%s>", name, checked ? " checked" : "");

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of text area (text)
void um_html_area_text(const char *name, const char *value, unsigned int cols, unsigned int rows)
{
  um_html_cell_begin();

  printf("<textarea name=\"%s\" cols=\"%u\" rows=\"%u\">\n%s</textarea>", name, cols, rows, value);

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of text area (file)
void um_html_area_file(const char *name, const char *filename, unsigned int cols, unsigned int rows)
{
  FILE                  *file_ptr;
  int                   ch;

  um_html_cell_begin();

  printf("<textarea name=\"%s\" cols=\"%u\" rows=\"%u\">\n", name, cols, rows);
  if ((file_ptr = fopen(filename, "r"))) {
    while ((ch = fgetc(file_ptr)) != EOF) {
      switch (ch) {
        case '&':
          printf("&amp;");
          break;
        case '<':
          printf("&lt;");
          break;
        case '>':
          printf("&gt;");
          break;
        default:
          putchar(ch);
      }
    }
    fclose(file_ptr);
  }
  printf("</textarea>");

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of submit button
void um_html_submit(const char *name, const char *value)
{
  um_html_cell_begin();

  printf("<input type=\"submit\" name=\"%s\" value=\"%s\">", name, value);

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of beginning of cell
static void um_html_pre_cell_begin(int header)
{
  if (!um_table_row) {
    printf("<table width=\"100%%\">");
    um_table_row++;
  }

  if (header) {
    if (um_table_id) {
      printf("<tr><td nowrap align=\"center\" class=\"header\" id=\"%s\">", um_table_id);
    } else {
      printf("<tr><td nowrap align=\"center\" class=\"header\">");
    }
  } else {
    if (um_table_id) {
      printf("<tr><td nowrap id=\"%s\"><pre>\n", um_table_id);
    } else {
      printf("<tr><td nowrap><pre>\n");
    }
  }

  um_table_id = NULL;
}

// **************************************************************************
// send HTML code of end of cell
static void um_html_pre_cell_end(int header)
{
  if (header) {
    printf("</td></tr>");
  } else {
    printf("</pre></td></tr>");
  }
}

// **************************************************************************
// send HTML code of header of preformatted block
void um_html_pre_head(const char *text)
{
  um_html_pre_cell_begin(1);

  printf("%s", text);

  um_html_pre_cell_end(1);
}

// **************************************************************************
// send HTML code of preformatted block (text)
void um_html_pre_text(const char *text)
{
  um_html_pre_cell_begin(0);

  printf("%s\n", text);

  um_html_pre_cell_end(0);
}

// **************************************************************************
// send HTML code of preformatted block (file)
void um_html_pre_file(const char *filename, const char *error)
{
  um_html_pre_cell_begin(0);

  um_http_file_content(filename, error);

  um_html_pre_cell_end(0);
}

// **************************************************************************
// send HTML code of preformatted block (program output)
void um_html_pre_proc(const char *program)
{
  um_html_pre_cell_begin(0);

  fflush(stdout);

  um_process_redirect_stdout(STDOUT_FILENO);
  um_process_exec("/bin/sh", "-c", program);

  um_html_pre_cell_end(0);
}

// **************************************************************************
// send HTML code of page with system log
void um_html_system_log(const char *title, const um_menu_item_t *menu)
{
  um_html_page_begin(title);

  um_html_form_begin(title, "System Log", "/slog_save.cgi", 0, NULL, menu);

  um_html_pre_head("System Messages");
  um_html_pre_proc("cat /var/log/messages.1 /var/log/messages 2> /dev/null | tail -n 25");

  um_html_form_break();

  um_html_table(2, 0);

  um_html_submit("button", "Save Log");
  um_html_submit("button", "Save Report");

  um_html_form_end(NULL);

  um_html_page_end();
}

