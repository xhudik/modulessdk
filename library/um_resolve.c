// **************************************************************************
//
// Function for resolving host IP address
//
// Copyright (C) 2015 Conel s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include <netdb.h>
#include <resolv.h>
#include <syslog.h>
#include <arpa/inet.h>

#include "um_resolve.h"

// **************************************************************************
// resolve host IP address
unsigned int um_resolve(const char *hostname)
{
  struct hostent        *host_ptr;
  struct in_addr        **addr_list;

  // read configuration files to get DNS server address
  res_init();

  // resolve host IP address
  host_ptr = gethostbyname(hostname);
  if (!host_ptr) {
    syslog(LOG_WARNING, "error resolving hostname %s", hostname);
    return 0;
  }

  // get array of pointers to addresses
  addr_list = (struct in_addr **)host_ptr->h_addr_list;

  // return resolved IP address
  return addr_list[0] ? ntohl(addr_list[0]->s_addr) : 0;
}

