// **************************************************************************
//
// Functions for getting parameters of CGI script
//
// Copyright (C) 2015 Conel s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "um_cgi.h"

// **************************************************************************

// name of uploaded file
#define UPLOAD_FILENAME     "/tmp/upload.bin"

// **************************************************************************

// size of table of allowed characters
#define MAX_SAFE_CHAR       128

// table of allowed characters
static const unsigned char SAFE_CHAR[MAX_SAFE_CHAR] = {
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  1,1,0,1,0,1,0,0,0,0,1,1,1,1,1,1,
  1,1,1,1,1,1,1,1,1,1,1,0,0,1,0,1,
  1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
  1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,
  0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
  1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,0
};

// **************************************************************************

// parameters of CGI script
static char *query = NULL;

// **************************************************************************
// convert character to number
static int char2int(char ch)
{
  return (ch >= 'A') ? ((ch & 0xDF) - 'A' + 10) : (ch - '0');
}

// **************************************************************************
// remove all escape sequences from string
static void unescape_string(char *str)
{
  char                  *src, *dst;
  int                   tmp;

  src = dst = str;
  while (*src) {
    if (*src == '%') {
      src++;
      tmp  = char2int(*src++) << 4;
      tmp += char2int(*src++);
    } else {
      tmp  = *src++;
    }
    *dst++ = (char)tmp;
  }
  *dst = '\0';
}

// **************************************************************************
// remove all control characters from string
static void unctrl_string(char *str)
{
  char                  *src, *dst, tmp;

  src = dst = str;
  while (*src) {
    tmp = *src++;
    if (((unsigned char)tmp < MAX_SAFE_CHAR) && SAFE_CHAR[(unsigned char)tmp]) {
      *dst++ = tmp;
    }
  }
  *dst = '\0';
}

// **************************************************************************
// remove all spaces from string
static void unspace_string(char *str)
{
  char                  *src, *dst, tmp;

  src = dst = str;
  while (*src) {
    tmp = *src++;
    if (tmp != ' ') {
      *dst++ = tmp;
    }
  }
  *dst = '\0';
}

// **************************************************************************
// get one line from standard input
static size_t get_line(char *line, size_t size, size_t *remain)
{
  size_t                count;
  int                   c;

  count = 0;
  while (*remain) {
    c = getchar();
    if (c == EOF) {
      break;
    }
    count++;
    (*remain)--;
    *line++ = (char)c;
    if (c == '\n') {
      break;
    }
    if (count >= size - 1) {
      break;
    }
  }

  *line = '\0';

  return count;
}

// **************************************************************************
// parse multipart data
static void um_cgi_parse_multipart(void)
{
  char                  buffer_act[4096], buffer_prev[4096];
  char                  *boundary, *name, *temp;
  size_t                count_act, count_prev, length, size;
  FILE                  *fp = NULL;

  // get content length
  if ((temp = getenv("CONTENT_LENGTH"))) {
    length = strtoul(temp, NULL, 10);
    // search for beginning of boundary
    if ((temp = getenv("CONTENT_TYPE"))) {
      if ((boundary = strstr(temp, "boundary="))) {
        // get boundary
        boundary += 9;
        // skip the first line
        get_line(buffer_act, sizeof(buffer_act), &length);
        // process remaining lines
        while ((get_line(buffer_act, sizeof(buffer_act), &length))) {
          // create file if data were received
          if ((name = strstr(buffer_act, "filename=\""))) {
            name += 10;
            strtok(name, "\"");
            if (strlen(name)) {
              query = malloc(strlen(name) + 64);
              sprintf(query, "filename=" UPLOAD_FILENAME "&name=%s", name);
              fp = fopen(UPLOAD_FILENAME, "w");
            }
          }
          // search for empty line
          while (get_line(buffer_act, sizeof(buffer_act), &length)) {
            if ((buffer_act[0] == '\r') && (buffer_act[1] == '\n'))
              break;
          }
          // process next part of request until boundary
          count_prev = 0;
          size       = 0;
          while ((count_act = get_line(buffer_act, sizeof(buffer_act), &length))) {
            if (!strstr(buffer_act, boundary)) {
              if (fp && count_prev) {
                fwrite(buffer_prev, 1, count_prev, fp);
                size += count_prev;
              }
              memcpy(buffer_prev, buffer_act, count_act);
              count_prev = count_act;
            } else {
              if (fp && count_prev > 2) {
                fwrite(buffer_prev, 1, count_prev - 2, fp);
                size += count_prev - 2;
              }
              break;
            }
          }
          // close file
          if (fp) {
            fclose(fp);
            fp = NULL;
          }
        }
      }
    }
  }
}

// **************************************************************************
// start CGI script parameters processing
void um_cgi_begin(void)
{
  char                  *request_method, *content_type, *str;
  size_t                length;

  // return if CGI script parameters have been already processed
  if (query) {
    return;
  }

  // get content type
  content_type = getenv("CONTENT_TYPE");

  // parse multipart data
  if (content_type && strstr(content_type, "multipart/form-data")) {
    um_cgi_parse_multipart();
    return;
  }

  // get request method
  if ((request_method = getenv("REQUEST_METHOD"))) {
    // process parameters passed by POST method
    if (!strcmp(request_method, "POST")) {
      if ((str = getenv("CONTENT_LENGTH"))) {
        length = strtoul(str, NULL, 10);
        query = malloc(length + 1);
        if (fread(query, 1, length, stdin) != length) {
          free(query);
          query = NULL;
          return;
        }
        query[length] = '\0';
      }
    // process parameters passed by GET method
    } else if (!strcmp(request_method, "GET")) {
      if ((str = getenv("QUERY_STRING"))) {
        query = strdup(str);
      }
    }
  }
}

// **************************************************************************
// finish CGI script parameters processing
void um_cgi_end(void)
{
  if (query) {
    free(query);
    query = NULL;
    unlink(UPLOAD_FILENAME);
  }
}

// **************************************************************************
// check if query is ok
int um_cgi_query_ok(void)
{
  return (query != NULL && *query) ? 1 : 0;
}

// **************************************************************************
// get CGI script parameter (raw string)
int um_cgi_get_raw(const char *name, char **value_ptr)
{
  char                  *param, *value, *src, *dst;

  if (query) {
    param = malloc(strlen(query) + 1);
    value = malloc(strlen(query) + 1);
    src   = query;
    while (*src) {
      dst = param;
      while (*src && (*src != '=')) {
        *dst++ = (char)((*src == '+') ? ' ' : *src);
        src++;
      }
      if (*src) src++;
      *dst = '\0';
      dst = value;
      while (*src && (*src != '&')) {
        *dst++ = (char)((*src == '+') ? ' ' : *src);
        src++;
      }
      if (*src) src++;
      *dst = '\0';
      unescape_string(param);
      if (!strcmp(param, name)) {
        free(param);
        unescape_string(value);
        *value_ptr = value;
        return 1;
      }
    }
    free(param);
    free(value);
  }

  *value_ptr  = malloc(1);
  **value_ptr = '\0';

  return 1;
}

// **************************************************************************
// get CGI script parameter (string)
int um_cgi_get_str(const char *name, char **value_ptr, int no_spaces)
{
  um_cgi_get_raw(name, value_ptr);
  unctrl_string(*value_ptr);
  if (no_spaces) {
    unspace_string(*value_ptr);
  }

  return 1;
}

// **************************************************************************
// get CGI script parameter (integer)
int um_cgi_get_int(const char *name, unsigned int *value_ptr)
{
  char                  *str;
  int                   result;

  um_cgi_get_str(name, &str, 1);
  result = sscanf(str, "%u", value_ptr) == 1;
  if (!result) {
    result = !*str;
    *value_ptr = 0;
  }
  free(str);

  return result;
}

// **************************************************************************
// get CGI script parameter (boolean)
int um_cgi_get_bool(const char *name, unsigned int *value_ptr)
{
  char                  *str;

  um_cgi_get_raw(name, &str);
  *value_ptr = !strcmp(str, "on");
  free(str);

  return 1;
}

// **************************************************************************
// get CGI script parameter (IP address)
int um_cgi_get_ip(const char *name, unsigned int *value_ptr, int zero)
{
  char                  *str;
  unsigned int          f0, f1, f2, f3;

  um_cgi_get_str(name, &str, 1);

  if (!*str) {
    *value_ptr = 0;
    free(str);
    return zero;
  }

  if (sscanf(str, "%u.%u.%u.%u", &f3, &f2, &f1, &f0) == 4) {
    if ((f0 | f1 | f2 | f3) <= 255) {
      *value_ptr = (f3 << 24) + (f2 << 16) + (f1 << 8) + f0;
      free(str);
      return *value_ptr || zero;
    }
  }

  free(str);

  return 0;
}

// **************************************************************************
// get CGI script parameter (MAC address)
int um_cgi_get_mac(const char *name, char **value_ptr, int zero)
{
  char                  ch;
  unsigned int          f0, f1, f2, f3, f4, f5;

  um_cgi_get_str(name, value_ptr, 1);

  if (!**value_ptr) {
    return zero;
  }

  if (sscanf(*value_ptr, "%x:%x:%x:%x:%x:%x%c", &f5, &f4, &f3, &f2, &f1, &f0, &ch) == 6) {
    if ((f0 | f1 | f2 | f3 | f4 | f5) <= 255) {
      return 1;
    }
  }

  return 0;
}

