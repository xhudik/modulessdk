// **************************************************************************
//
// Functions for work with lock files
//
// Copyright (C) 2015 Conel s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#ifndef _UM_LOCK_H_
#define _UM_LOCK_H_

#ifdef __cplusplus
extern "C" {
#endif

// acquire lock
extern int um_lock_acquire(const char *device, int tout);

// release lock
extern void um_lock_release(const char *device);

#ifdef __cplusplus
}
#endif

#endif

