// **************************************************************************
//
// Functions for work with processes
//
// Copyright (C) 2015 Conel s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "um_process.h"
#include "um_macros.h"

// **************************************************************************

// redirected file descriptors
static __thread int fd_stdin  = -1;
static __thread int fd_stdout = -1;
static __thread int fd_stderr = -1;

// **************************************************************************
// redirect stdin for the next executed process
void um_process_redirect_stdin(int fd)
{
  if (fd != STDOUT_FILENO && fd != STDERR_FILENO) {
    fd_stdin = fd;
  }
}

// **************************************************************************
// redirect stdout for the next executed process
void um_process_redirect_stdout(int fd)
{
  if (fd != STDIN_FILENO && fd != STDERR_FILENO) {
    fd_stdout = fd;
  }
}

// **************************************************************************
// redirect stderr for the next executed process
void um_process_redirect_stderr(int fd)
{
  if (fd != STDIN_FILENO && fd != STDOUT_FILENO) {
    fd_stderr = fd;
  }
}

// **************************************************************************
// close all redirected file descriptors and cancel redirection
static void um_process_close_redirects(void)
{
  // close file descriptors
  if (fd_stdin > STDERR_FILENO) {
    close(fd_stdin);
  }
  if (fd_stdout > STDERR_FILENO) {
    close(fd_stdout);
  }
  if (fd_stderr > STDERR_FILENO) {
    close(fd_stderr);
  }

  // cancel redirection for the next executed process
  fd_stdin  = -1;
  fd_stdout = -1;
  fd_stderr = -1;
}

// **************************************************************************
// replace the current process with a new process
static void um_process_replace(const char *program, va_list ap)
{
  char                  *argv[64];
  unsigned int          i;
  int                   fd_null;
  int                   fd;

  // open "/dev/null"
  fd_null = open("/dev/null", O_RDWR);

  // redirect stdin
  if (fd_stdin != STDIN_FILENO) {
    close(STDIN_FILENO);
    if (fd_stdin >= 0) {
      dup2(fd_stdin, STDIN_FILENO);
    } else if (fd_null >= 0) {
      dup2(fd_null, STDIN_FILENO);
    }
  }

  // redirect stdout
  if (fd_stdout != STDOUT_FILENO) {
    close(STDOUT_FILENO);
    if (fd_stdout >= 0) {
      dup2(fd_stdout, STDOUT_FILENO);
    } else if (fd_null >= 0) {
      dup2(fd_null, STDOUT_FILENO);
    }
  }

  // redirect stderr
  if (fd_stderr != STDERR_FILENO) {
    close(STDERR_FILENO);
    if (fd_stderr >= 0) {
      dup2(fd_stderr, STDERR_FILENO);
    } else if (fd_null >= 0) {
      dup2(fd_null, STDERR_FILENO);
    }
  }

  // close all file descriptors except stdin, stdout and stderr
  for (fd = getdtablesize() - 1; fd > STDERR_FILENO; fd--) {
    close(fd);
  }

  // prepare argument list
  argv[0] = strdup(program);
  for (i = 1; i + 1 < SIZEOF(argv); i++) {
    if ((argv[i] = va_arg(ap, char *)) == NULL) {
      break;
    }
  }
  argv[i] = NULL;

  // replace the current process with a new process
  execvp(argv[0], argv);
}

// **************************************************************************
// start a process and wait for its completion
int (um_process_exec)(const char *program, ...)
{
  va_list               ap;
  pid_t                 pid;
  int                   status;

  pid = fork();
  if (pid == 0) {
    va_start(ap, program);
    um_process_replace(program, ap);
    va_end(ap);
    _exit(1);
  } else if (pid > 0) {
    um_process_close_redirects();
    waitpid(pid, &status, 0);
    return status;
  } else {
    um_process_close_redirects();
    return -1;
  }
}

// **************************************************************************
// start a process in the background
pid_t (um_process_start)(const char *program, ...)
{
  va_list               ap;
  pid_t                 pid;

  pid = fork();
  if (pid == 0) {
    va_start(ap, program);
    um_process_replace(program, ap);
    va_end(ap);
    _exit(1);
  } else {
    um_process_close_redirects();
    return pid;
  }
}

// **************************************************************************
// send a signal to process running in the background
int um_process_kill(pid_t pid, int sig)
{
  if (pid > 0) {
    return kill(pid, sig);
  } else {
    errno = ESRCH;
    return -1;
  }
}

// **************************************************************************
// check existence of process running in the background
int um_process_exists(pid_t pid)
{
  return um_process_kill(pid, 0) == 0 || errno != ESRCH;
}

