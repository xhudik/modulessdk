// **************************************************************************
//
// Functions for work with serial port
//
// Copyright (C) 2015 Conel s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#ifndef _UM_COM_H_
#define _UM_COM_H_

#ifdef __cplusplus
extern "C" {
#endif

// open serial port
extern int um_com_open(const char *device, unsigned int baudrate, unsigned int databits, const char *parity, unsigned int stopbits);

// close serial port
extern void um_com_close(int fd);

// send and receive data
extern char *um_com_xmit(int fd, const char *str, int timeout);

// wait until transmitter is empty
extern void um_com_drain(int fd);

// set state of RTS signal
extern void um_com_set_rts(int fd, int state);

// set state of DTR signal
extern void um_com_set_dtr(int fd, int state);

// get state of CTS signal
extern int um_com_get_cts(int fd);

// get state of DSR signal
extern int um_com_get_dsr(int fd);

// get state of CD signal
extern int um_com_get_cd(int fd);

#ifdef __cplusplus
}
#endif

#endif

