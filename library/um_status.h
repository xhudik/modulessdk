// **************************************************************************
//
// Functions for reading out status
//
// Copyright (C) 2017 Conel s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#ifndef _UM_STATUS_H_
#define _UM_STATUS_H_

#ifdef __cplusplus
extern "C" {
#endif

// get the value of the given status parameter
extern char *um_status_get(const char *category, const char *param);

// get the product name
extern const char *um_status_get_product_name(void);

// get the firmware version
extern const char *um_status_get_firmware_version(void);

// get the serial number
extern const char *um_status_get_serial_number(void);

// get the model of the given module
extern const char *um_status_get_module_model(int module);

// get the revision of the given module
extern const char *um_status_get_module_revision(int module);

// get the IMEI of the given module
extern const char *um_status_get_module_imei(int module);

// get the ICCID of the given module
extern const char *um_status_get_module_iccid(int module);

//  get the ESN of the given module
extern const char *um_status_get_module_esn(int module);

// get the MEID of the given module
extern const char *um_status_get_module_meid(int module);

// get the interface of the given module
extern const char *um_status_get_module_iface(int module);

// get the registration state of the given module
extern const char *um_status_get_mobile_registration(int module);

// get the operator of the given module
extern const char *um_status_get_mobile_operator(int module);

// get the technology of the given module
extern const char *um_status_get_mobile_technology(int module);

// get the PLMN of the given module
extern unsigned int um_status_get_mobile_plmn(int module);

// get the cell of the given module
extern unsigned int um_status_get_mobile_cell(int module);

// get the LAC of the given module
extern unsigned int um_status_get_mobile_lac(int module);

// get the channel of the given module
extern unsigned int um_status_get_mobile_channel(int module);

// get the signal strength of the given module
extern int um_status_get_mobile_strength(int module);

// get the signal quality of the given module
extern int um_status_get_mobile_quality(int module);

#ifdef __cplusplus
}
#endif

#endif

