// **************************************************************************
//
// Functions for work with serial port
//
// Copyright (C) 2015 Conel s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <syslog.h>
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/select.h>

#include "um_com.h"

// **************************************************************************
// get constant for setting baudrate
static unsigned int um_com_baudrate(unsigned int baudrate)
{
  switch (baudrate) {
    case 300:
      return B300;
    case 600:
      return B600;
    case 1200:
      return B1200;
    case 2400:
      return B2400;
    case 4800:
      return B4800;
    case 9600:
      return B9600;
    case 19200:
      return B19200;
    case 38400:
      return B38400;
    case 57600:
      return B57600;
    case 115200:
      return B115200;
    case 230400:
      return B230400;
    case 460800:
      return B460800;
    case 500000:
      return B500000;
    case 576000:
      return B576000;
    case 921600:
      return B921600;
    case 1000000:
      return B1000000;
    case 1152000:
      return B1152000;
    case 1500000:
      return B1500000;
    case 2000000:
      return B2000000;
    case 2500000:
      return B2500000;
    case 3000000:
      return B3000000;
    case 3500000:
      return B3500000;
    case 4000000:
      return B4000000;
  }

  return B9600;
}

// **************************************************************************
// open serial port
int um_com_open(const char *device, unsigned int baudrate, unsigned int databits, const char *parity, unsigned int stopbits)
{
  struct termios        term;
  unsigned int          cflags;
  int                   fd;

  // open serial port
  if ((fd = open(device, O_RDWR | O_NOCTTY | O_NONBLOCK | O_FSYNC)) < 0) {
    syslog(LOG_ERR, "open(%s) error: %s", device, strerror(errno));
    return -1;
  }

  // prepare parameters of serial port
  cflags  = um_com_baudrate(baudrate);
  cflags |= (databits == 7) ? CS7 : CS8;
  cflags |= (stopbits == 2) ? CSTOPB : 0;
  cflags |= (parity[0] == 'O') ? PARENB | PARODD : 0;
  cflags |= (parity[0] == 'E') ? PARENB : 0;

  // set parameters of serial port
  tcgetattr(fd, &term);
  term.c_cflag = HUPCL | CLOCAL | CREAD | cflags;
  term.c_iflag = IGNBRK | IGNPAR;
  term.c_lflag = 0;
  term.c_oflag = 0;
  if (tcsetattr(fd, TCSANOW, &term) != 0) {
    syslog(LOG_ERR, "tcsetattr(%s) error: %s", device, strerror(errno));
  }

  // return file descriptor
  return fd;
}

// **************************************************************************
// close serial port
void um_com_close(int fd)
{
  if (close(fd) != 0) {
    syslog(LOG_ERR, "close error: %s", strerror(errno));
  }
}

// **************************************************************************
// send and receive data
char *um_com_xmit(int fd, const char *str, int timeout)
{
  static __thread char  buffer[8192];
  char                  temp[128];
  struct timeval        tv;
  fd_set                rfds;
  ssize_t               count;
  size_t                size;

  // send data to serial port
  if (write(fd, str, strlen(str)) < 0) {
    syslog(LOG_ERR, "write error: %s", strerror(errno));
  }

  // prepare buffer for data
  size = 0;
  memset(buffer, 0, sizeof(buffer));

  // receive data from serial port
  FD_ZERO(&rfds);
  FD_SET(fd, &rfds);
  tv.tv_sec  = timeout;
  tv.tv_usec = 0;
  while (select(fd + 1, &rfds, NULL, NULL, &tv) > 0) {
    if (size < sizeof(buffer) - 1) {
      count = read(fd, buffer + size, sizeof(buffer) - size - 1);
    } else {
      count = read(fd, temp, sizeof(temp));
    }
    if (count > 0) {
      size += (size_t)count;
    }
    if (count <= 0 || size >= 65536) {
      break;
    }
    if (count <= 2) {
      tv.tv_sec  = timeout;
      tv.tv_usec = 0;
    } else {
      tv.tv_sec  = 0;
      tv.tv_usec = 200000;
    }
  }

  // return received data
  return buffer;
}

// **************************************************************************
// wait until transmitter is empty
void um_com_drain(int fd)
{
  int                   status;

  do {
    if (ioctl(fd, TIOCSERGETLSR, &status) != 0) {
      syslog(LOG_ERR, "ioctl(TIOCSERGETLSR) error: %s", strerror(errno));
      break;
    }
  } while (!(status & TIOCSER_TEMT));
}

// **************************************************************************
// set state of RTS signal
void um_com_set_rts(int fd, int state)
{
  int                   status;

  status = TIOCM_RTS;
  if (ioctl(fd, state ? TIOCMBIS : TIOCMBIC, &status) != 0) {
    syslog(LOG_ERR, "ioctl(TIOCMBIx) error: %s", strerror(errno));
  }
}

// **************************************************************************
// set state of DTR signal
void um_com_set_dtr(int fd, int state)
{
  int                   status;

  status = TIOCM_DTR;
  if (ioctl(fd, state ? TIOCMBIS : TIOCMBIC, &status) != 0) {
    syslog(LOG_ERR, "ioctl(TIOCMBIx) error: %s", strerror(errno));
  }
}

// **************************************************************************
// get state of CTS signal
int um_com_get_cts(int fd)
{
  int                   status;

  if (ioctl(fd, TIOCMGET, &status) != 0) {
    syslog(LOG_ERR, "ioctl(TIOCMGET) error: %s", strerror(errno));
    return 0;
  }

  return status & TIOCM_CTS ? 1 : 0;
}

// **************************************************************************
// get state of DSR signal
int um_com_get_dsr(int fd)
{
  int                   status;

  if (ioctl(fd, TIOCMGET, &status) != 0) {
    syslog(LOG_ERR, "ioctl(TIOCMGET) error: %s", strerror(errno));
    return 0;
  }

  return status & TIOCM_DSR ? 1 : 0;
}

// **************************************************************************
// get state of CD signal
int um_com_get_cd(int fd)
{
  int                   status;

  if (ioctl(fd, TIOCMGET, &status) != 0) {
    syslog(LOG_ERR, "ioctl(TIOCMGET) error: %s", strerror(errno));
    return 0;
  }

  return status & TIOCM_CD ? 1 : 0;
}

