# default platform
PLATFORM ?= v2

# platform specific settings
ifeq ($(PLATFORM),pc)
ARCH             := x86_64
HOST             := x86_64-linux-gnu
TOOLCHAIN_PATH   := /usr
TOOLCHAIN_PREFIX :=
TOOLCHAIN_CFLAGS :=
KERNEL_HEADERS   := /lib/modules/$(shell uname -r)/build/include
else ifeq ($(PLATFORM),v2)
ARCH             := arm
HOST             := arm-linux
TOOLCHAIN_PATH   := /opt/toolchain/gcc-conel-armv5-linux-gnueabi
TOOLCHAIN_PREFIX := armv5-linux-gnueabi-
TOOLCHAIN_CFLAGS := -mtls-dialect=gnu2 -fno-delete-null-pointer-checks
KERNEL_HEADERS   := $(TOOLCHAIN_PATH)/sysroot/usr/include
else ifeq ($(PLATFORM),v3)
ARCH             := arm
HOST             := arm-linux
TOOLCHAIN_PATH   := /opt/toolchain/gcc-conel-armv7-linux-gnueabi
TOOLCHAIN_PREFIX := armv7-linux-gnueabi-
TOOLCHAIN_CFLAGS := -mtls-dialect=gnu2 -fno-delete-null-pointer-checks
KERNEL_HEADERS   := $(TOOLCHAIN_PATH)/sysroot/usr/include
else ifeq ($(PLATFORM),v4)
ARCH             := arm64
HOST             := aarch64-linux
TOOLCHAIN_PATH   := /opt/toolchain/gcc-conel-aarch64-linux-gnu
TOOLCHAIN_PREFIX := aarch64-linux-gnu-
TOOLCHAIN_CFLAGS := -fno-delete-null-pointer-checks
KERNEL_HEADERS   := $(TOOLCHAIN_PATH)/sysroot/usr/include
else
$(error Unsupported platform "$(PLATFORM)")
endif

# build platform
BUILD    := x86_64-linux-gnu

# top directory
SDKDIR   := $(abspath $(dir $(lastword $(MAKEFILE_LIST))))

# subdirectory for object files
OBJDIR   := obj.$(PLATFORM)
OBJDIRS  := obj.*

# ccache command
CCACHE   := $(shell command -v ccache 2> /dev/null)

# toolchain commands
AR       := $(TOOLCHAIN_PATH)/bin/$(TOOLCHAIN_PREFIX)ar
AS       := $(TOOLCHAIN_PATH)/bin/$(TOOLCHAIN_PREFIX)as
CC       := $(TOOLCHAIN_PATH)/bin/$(TOOLCHAIN_PREFIX)gcc
CPP      := $(TOOLCHAIN_PATH)/bin/$(TOOLCHAIN_PREFIX)cpp
CXX      := $(TOOLCHAIN_PATH)/bin/$(TOOLCHAIN_PREFIX)g++
LD       := $(TOOLCHAIN_PATH)/bin/$(TOOLCHAIN_PREFIX)ld
NM       := $(TOOLCHAIN_PATH)/bin/$(TOOLCHAIN_PREFIX)nm
OBJCOPY  := $(TOOLCHAIN_PATH)/bin/$(TOOLCHAIN_PREFIX)objcopy
OBJDUMP  := $(TOOLCHAIN_PATH)/bin/$(TOOLCHAIN_PREFIX)objdump
RANLIB   := $(TOOLCHAIN_PATH)/bin/$(TOOLCHAIN_PREFIX)ranlib
READELF  := $(TOOLCHAIN_PATH)/bin/$(TOOLCHAIN_PREFIX)readelf
SIZE     := $(TOOLCHAIN_PATH)/bin/$(TOOLCHAIN_PREFIX)size
STRIP    := $(TOOLCHAIN_PATH)/bin/$(TOOLCHAIN_PREFIX)strip

# generic flags for C/C++ compiler
CFLAGS   += $(TOOLCHAIN_CFLAGS) -ffunction-sections -fPIC -Wall -Wextra -Wshadow -Wmissing-declarations
CXXFLAGS += $(TOOLCHAIN_CFLAGS) -ffunction-sections -fPIC -Wall -Wextra -Wshadow -Wmissing-declarations -Woverloaded-virtual
CPPFLAGS += -I$(SDKDIR)/library -include um_compat.h -MMD -MP
LDFLAGS  += -L$(SDKDIR)/library/$(OBJDIR) -L$(SDKDIR)/library/obj.$(PLATFORM) -Wl,--as-needed -Wl,--gc-sections
LDLIBS   += -l:libum.a -lrt

# extra flags for additional C/C++ compiler warnings
ifneq (,$(filter $(WARN),1 2))
OBJDIR   := $(OBJDIR).warn
CFLAGS   += -Wformat=2 -Wwrite-strings -Wfloat-equal -Wold-style-definition
CXXFLAGS += -Wformat=2 -Wwrite-strings -Wfloat-equal -Wold-style-cast
endif
ifneq (,$(filter $(WARN),2))
OBJDIR   := $(OBJDIR)2
CFLAGS   += -Wcast-align -Wcast-qual -Wconversion -Wpointer-arith
CXXFLAGS += -Wcast-align -Wcast-qual -Wconversion -Wpointer-arith
endif

# extra flags for address sanitizer
ifeq ($(ASAN),1)
OBJDIR   := $(OBJDIR).asan
CFLAGS   += -fsanitize=address -fno-omit-frame-pointer
CXXFLAGS += -fsanitize=address -fno-omit-frame-pointer
LDFLAGS  += -fsanitize=address -static-libasan
endif

# extra flags for undefined behavior sanitizer
ifeq ($(UBSAN),1)
OBJDIR   := $(OBJDIR).ubsan
CFLAGS   += -fsanitize=undefined -fno-omit-frame-pointer
CXXFLAGS += -fsanitize=undefined -fno-omit-frame-pointer
LDFLAGS  += -fsanitize=undefined -static-libubsan
endif

# extra flags for stack smashing protector
ifeq ($(SSP),1)
OBJDIR   := $(OBJDIR).ssp
CFLAGS   += -fstack-protector-strong
CXXFLAGS += -fstack-protector-strong
endif

# extra flags for debug and release build
ifeq ($(DEBUG),1)
OBJDIR   := $(OBJDIR).debug
CFLAGS   += -g -DDEBUG
CXXFLAGS += -g -DDEBUG
else
CFLAGS   += -O2
CXXFLAGS += -O2
LDFLAGS  += -s
endif

# rules for building C/C++ program
define build-program
all: $(OBJDIR)/$(strip $1)

$(OBJDIR)/$(strip $1): $(foreach EXT, c cc cpp, $(patsubst %.$(EXT), $(OBJDIR)/%.o, $(filter %.$(EXT), $2)))
	@echo "  LD  $$(@F)"
	@$(CXX) $(LDFLAGS) -o $$@ $$^ $3 $(LDLIBS)

-include $(foreach EXT, c cc cpp, $(patsubst %.$(EXT), $(OBJDIR)/%.d, $(filter %.$(EXT), $2)))

$(build-objects)
endef

# rules for building C/C++ shared library
define build-shared-library
all: $(OBJDIR)/$(strip $1)

$(OBJDIR)/$(strip $1): $(foreach EXT, c cc cpp, $(patsubst %.$(EXT), $(OBJDIR)/%.o, $(filter %.$(EXT), $2)))
	@echo "  LD  $$(@F)"
	@$(CXX) $(LDFLAGS) -shared -o $$@ $$^ $3 $(filter-out $(patsubst %.so, -l:%.a, $1), $(LDLIBS))

-include $(foreach EXT, c cc cpp, $(patsubst %.$(EXT), $(OBJDIR)/%.d, $(filter %.$(EXT), $2)))

$(build-objects)
endef

# rules for building C/C++ static library
define build-static-library
all: $(OBJDIR)/$(strip $1)

$(OBJDIR)/$(strip $1): $(foreach EXT, c cc cpp, $(patsubst %.$(EXT), $(OBJDIR)/%.o, $(filter %.$(EXT), $2)))
	@echo "  AR  $$(@F)"
	@$(AR) rcs $$@ $$^

-include $(foreach EXT, c cc cpp, $(patsubst %.$(EXT), $(OBJDIR)/%.d, $(filter %.$(EXT), $2)))

$(build-objects)
endef

# rules for building object files
define build-objects
$(OBJDIR):
	@mkdir -p $(OBJDIR)

$(OBJDIR)/%.o: %.c | $(OBJDIR) $(DEPENDS)
	@echo "  CC  $$(@F)"
	@$(CCACHE) $(CC) $(CPPFLAGS) $(CFLAGS) -c $$< -o $$@

$(OBJDIR)/%.o: %.cc | $(OBJDIR) $(DEPENDS)
	@echo "  CXX $$(@F)"
	@$(CCACHE) $(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $$< -o $$@

$(OBJDIR)/%.o: %.cpp | $(OBJDIR) $(DEPENDS)
	@echo "  CXX $$(@F)"
	@$(CCACHE) $(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $$< -o $$@

clean:
	@rm -rf $(OBJDIRS)

build-objects := $(UNDEFINED)
endef

# rules for building package
define build-package
.NOTPARALLEL:

all: check $$(PKGNAME).$$(PLATFORM).pkg
	@[ -s $$(PKGNAME).$$(PLATFORM).pkg ] || rm -f $$(PKGNAME).$$(PLATFORM).pkg

check:
	@$(foreach CMD, $(CMD_LIST), \
		if ! command -v $(CMD) > /dev/null; then \
			echo "  WARNING: Package $$(PKGNAME).$$(PLATFORM).pkg cannot be built."; \
			echo "  WARNING: Program \"$(CMD)\" not found."; \
			touch $$(PKGNAME).$$(PLATFORM).pkg; \
			exit 0; \
		fi; \
	) \
	$(foreach DIR, $(DIR_LIST), \
		if [ ! -d $(DIR) ]; then \
			echo "  WARNING: Package $$(PKGNAME).$$(PLATFORM).pkg cannot be built."; \
			echo "  WARNING: Directory $(abspath $(strip $(DIR))) not found."; \
			touch $$(PKGNAME).$$(PLATFORM).pkg; \
			exit 0; \
		fi; \
	) \
	$(foreach PKG, $(PKG_LIST), \
		$$(MAKE) -C $(dir $(PKG)); \
		if [ ! -e $(PKG) ]; then \
			echo "  WARNING: Package $$(PKGNAME).$$(PLATFORM).pkg cannot be built."; \
			touch $$(PKGNAME).$$(PLATFORM).pkg; \
			exit 0; \
		fi; \
	)

$$(PKGNAME).$$(PLATFORM).pkg: Makefile $(wildcard *.patch $(PKG_LIST))
endef

# rules for building module
define build-module
MODNAME ?= $(notdir $(CURDIR))
TEMPDIR := $(CURDIR)/tmp
DESTDIR := $(CURDIR)/tmp/$$(MODNAME)
IMGDIR  ?= .

ifeq ($(REQUIRE)$(INSTALL)$(INCLUDE)$(COMPILE)$(EXECUTE),)
all: build
else
all:
ifeq ($$(IMGDIR),.)
	@$(foreach FILE, $(REQUIRE) $(INSTALL), \
		$$(MAKE) -C $(dir $(FILE)); \
	)
endif
	@$(foreach FILE, $(REQUIRE) $(INSTALL) $(INCLUDE) $(COMPILE), \
		if [ ! -e $(FILE) ]; then \
			echo "  WARNING: Module $$(MODNAME).$$(PLATFORM).tgz cannot be built."; \
			echo "  WARNING: Component $(abspath $(strip $(FILE))) not found."; \
			exit 0; \
		fi; \
	) \
	$(foreach FILE, $(EXECUTE), \
		if ! command -v $(FILE) > /dev/null; then \
			echo "  WARNING: Module $$(MODNAME).$$(PLATFORM).tgz cannot be built."; \
			echo "  WARNING: Program \"$(FILE)\" not found."; \
			exit 0; \
		fi; \
	) \
	$$(MAKE) build
endif

build:
	@rm -rf $$(TEMPDIR)
	@mkdir -p $$(DESTDIR) $$(IMGDIR)
	@$(foreach FILE, $(INSTALL), tar -xf $(FILE) -C $$(DESTDIR);)
	@$(foreach DIR, $(INCLUDE) $(wildcard merge), cp -rf $(DIR)/* $$(DESTDIR);)
	@$(foreach DIR, $(COMPILE) $(wildcard source), $$(MAKE) -C $(DIR);)
	@$(foreach DIR, $(COMPILE) $(wildcard source), $$(MAKE) -C $(DIR) install DESTDIR=$$(DESTDIR);)
	@tar -c --owner=0 --group=0 --mtime="2001-01-01 UTC" --exclude-vcs -C $$(TEMPDIR) $$(MODNAME) | gzip -n > $$(IMGDIR)/$$(MODNAME).$$(PLATFORM).tgz
	@rm -rf $$(TEMPDIR)

clean:
	@$(foreach DIR, $(COMPILE) $(wildcard source), $$(MAKE) -C $(DIR) clean;)
	@rm -rf $$(TEMPDIR) $$(MODNAME).*.tgz

$(upload-module)
endef

# rules for uploading module
define upload-module
ROUTER   ?= 192.168.1.1
USERNAME ?= root
PASSWORD ?= root

upload: build
	@mkdir -p $$(TEMPDIR)
	@curl -k -c $$(TEMPDIR)/cookies -o /dev/null -d "username=$$(USERNAME)&password=$$(PASSWORD)" https://$$(ROUTER)/login_exec.cgi
	@curl -k -b $$(TEMPDIR)/cookies -o $$(TEMPDIR)/output https://$$(USERNAME):$$(PASSWORD)@$$(ROUTER)/module.cgi
	@curl -k -b $$(TEMPDIR)/cookies -o /dev/null -F "request_id=`sed -n 's/.*name=\"request_id\" value=\"\([^\"]*\).*/\1/p' $$(TEMPDIR)/output`" -F "module=@$$(MODNAME).$$(PLATFORM).tgz" https://$$(USERNAME):$$(PASSWORD)@$$(ROUTER)/module_add.cgi
	@rm -rf $$(TEMPDIR)
endef

# rules for missing compiler
ifeq ($(wildcard $(CC)),)
skip:
	@echo "  WARNING: Compiler $(CC) not found."
endif
